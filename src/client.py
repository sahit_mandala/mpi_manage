import mpi4py

#all roles
ROLE_NAME=1  #name node
ROLE_DATA=2  #data node
ROLE_DATA_LEADER=3  #a data-node's leader
ROLE_CLIENT=4

MPI_ROOT=0

PROC_DIE_FLAG=0
PROC_LIVE_FLAG=1

M_REG_PUSH

M_GET_DN
M_GET_DATA
M_PUSH
M_REQ_PUSH
M_PUSH_DATA

M_REG_APP_ID
MPI_STATUS_IGNORE

class dm_client():
    
    def __init__(self,debug=False):
        # Create a data management client instance.
        # Needs cred on main server nodes, codes to access 
        # memory directly

        hostname = MPI.get_processor_name()
        comm_world = MPI.COMM_WORLD
        w_rank = comm_world.Get_rank()
        w_group = comm_world.Get_group()
        total_comm_procs = comm_world.Get_size() 
        total_group_procs = w_group.Get_size()
        
        #No real use on client side, only to cooperate with server side: find local-leader on each server node
        #all_procs_hostnames = ["0" for i in range(0,total_comm_procs+1)]
        all_procs_hostnames = comm_world.allgather(hostname)
        #free(all_procs_hostname)
        
        #Form new global communicator to gather all gloabal proc's live-die flag
        #unnecessary procs quuit on server side
        #TODO
        live_flag = PROC_LIVE_FLAG
        #all_procs = [0 for i in range(0,total_comm_procs+1)]
        all_procs=comm_world.allgather(live_flag)
        live_count = 0
        for i in all_procs:
            if (not (i == PROC_DIE_FLAG)):
                live_count +=1
                
        live_procs = [0 for i in range(0,live_count)]
        j=0
        for i in range(0,total_group_procs):
            if (not(all_procs[i]==PROC_DIE_FLAG)):
                live_procs[j]=i
                j+=1
                
        nw_group = w_group.incl(live_procs)
        nw_comm = comm_world.create(nw_group)
        #free(live_procs)
        
        #New group comm
        total_comm_procs = nw_comm.Get_size()
        rank = nw_comm.Get_rank()
        
        #all_roles = ["0" for i in range(0,total_comm_procs) ]
        all_roles = new_comm.allgather(ROLE_CLIENT)
        staging_name = []
        staging_data = []
        client_procs = []

        #ct translates to self
        self.nw_rank = rank
        for i,v in enumerate(all_roles):
            if (v==ROLE_NAME):
                staging_name.append(i)
            if (v==ROLE_DAT):
                staging_data.append(i)
            if (v==ROLE_DATA)
                
        w_group.incl(live_procs)
        client_comm = nw_comm.Comm_create(client_group)
        client_trade = client_comm.Comm_rank()
        MPI.Group_free(w_group)
        
        
        self = {}
        self.nw_group = nw_group
        self.rank = rank
        self.nw_comm = nw_comm
        self.staging_name = staging_name
        self.staging_data = staging_data
        self.client_procs = client_procs
        
    def register_app(self):
		if (self.rank == MPI_ROOT):
			self.nw_comm.send([M_REG_APP_ID], self.staging_name[0], M_REG_APP_ID)
			app_id = self.nw_comm.recv(self.staging_name[0], M_REG_APP_ID)
		self.client_comm.barrier();
		app_id = self.client_comm.bcast(obj=app_id, root=MPI_ROOT)
		
		return app_id
		
		
    
        
    def get(self,key):
        #Given a key, calls the NN server, retreives data on key
        nn_rank = self.staging_name[0%len(self.staging_name)]


#IDEA: Potentially cache/pass info about data location to dependent tasks
		#Get dn where data is located
        self.nw_comm.send(key,nn_rank,tag=M_GET_DN)
        dn_rank = self.nw_comm.recv(nn_rank, tag=MPI_STATUS_IGNORE)
        
        req_data={'key':key,'rank':self.rank}
        self.nw_comm.send(req_data, dest=dn_rank,tag=M_GET_DATA)
        
        data = self.nw_comm.recv(source=dn_rank, tag=M_GET_DATA)
        
        return data
        

    def put(self,data):
        #Give a data structure, pushes to the staging area, returns key to access
   
        #Hash the key to determine specific nn to use
        nn_rank = self.staging_name[0%len(self.staging_name)]
        
        #encodes metadata, data to store
        push_data = {'key':key, 'data':data}
        
        #info for name node
        reg_data = {'key':key,'size':sys.getsizeof(push_data)}
        
        self.nw_comm.send(reg_data,nn_rank,tag=M_REQ_PUSH)
        dn_rank = self.nw_comm.recv(nn_rank, tag=MPI_STATUS_IGNORE)
        
        if (dn_rank<0):
			print("Error: Negative rank")
			raise Exception
		
		self.nw_comm.send(push_data, dest=dn_rank,tag=M_PUSH_DATA)
		#self.nw_comm.send(ACTUAL_DATA, dest=dn_rank,tag=M_PUSH_DATA)
		return 0
		
		
			
		
        
