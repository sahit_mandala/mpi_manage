from mpi4py import MPI

class NameNode():
# Manages the put/get requests
    def __init__(self):
        hostname = MPI.get_processor_name()
        comm_world = MPI.COMM_WORLD
        w_rank = comm_world.Get_rank()
        w_group = comm_world.Get_group()
        total_comm_procs = comm_world.Get_size() 
        total_group_procs = w_group.Get_size()
        
        #No real use on client side, only to cooperate with server side: find local-leader on each server node
        #all_procs_hostnames = ["0" for i in range(0,total_comm_procs+1)]
        live_flag = PROC_LIVE_FLAG
        all_procs_hostnames = comm_world.allgather(hostname)
        t_node_leader = total_comm_procs 
        for i in range(0,total_comm_procs):
            if (all_procs_hostnames[i] == hostname):
                    t_node_leader = min(t_node_leader, i)
        if (role_id==ROLE_NAME and w_rank!=t_node_leader):
            live_flag=PROC_DIE_FLAG
        if (role_id==ROLE_NAME and w_rank==t_node_leader):
            role_id = ROLE_DATA_LEADER
            
        #free(all_procs_hostname)
        
        #Form new global communicator to gather all gloabal proc's live-die flag
        #unnecessary procs quuit on server side
        #TODO
        #all_procs = [0 for i in range(0,total_comm_procs+1)]
        all_procs=comm_world.allgather(live_flag)
        live_count = 0
        for i in all_procs:
            if (not (i == PROC_DIE_FLAG)):
                live_count +=1
                
        live_procs = [0 for i in range(0,live_count)]
        j=0
        for i in range(0,total_group_procs):
            if (not(all_procs[i]==PROC_DIE_FLAG)):
                live_procs[j]=i
                j+=1
                
        nw_group = w_group.incl(live_procs)
        nw_comm = comm_world.create(nw_group)
        
        #if redundant procs exit before this create-operation, it will stuck there
        if (not live_flag):
            MPI.Finalize()
            exit(0)
            
        #New group comm
        total_comm_procs = nw_comm.Get_size()
        rank = nw_comm.Get_rank()
        
        #all_roles = ["0" for i in range(0,total_comm_procs) ]
        all_roles = new_comm.allgather(role_id)

        #ct translates to self
        self.nw_rank = rank
        if (role_id == ROLE_NAME):
            staging_data = []
            staging_data_leader_count = 0
            for i,v in enumerate(all_roles):
                if (v==ROLE_DATA):
                    staging_data.append(i)
                else if (v==ROLE_DATA_LEADER):
                    staging_data.append(i)
                    staging_data_leader_count+=1
        
        staging_name = []
        client_procs = []
        for i,v in enumerate(all_roles):
                if (v==ROLE_NAME):
                    staging_name.append(i)
                else if (v<ROLE_NAME or v>ROLE_DATA_LEADER):
                    client_procs.append(i)
        

        client_group = nw_group.incl(client_procs)
        client_comm = nw_comm.Comm_create(client_group)
        client_trade = client_comm.Get_rank()
        #MPI.Group_free(w_group)
        
        st = {}
        st.nw_group = nw_group
        st.rank = rank
        st.nw_comm = nw_comm
        st.staging_name = staging_name
        ct.staging_data = staging_data
        ct.client_procs = client_procs

        
        
        #Find out redundant procs for name-node, there should be only one MPI process, mark processes that are unnecessary
        
        
        #Form new global communicator; Unnecessary procs quit:

        
class StagingNode():

    def __init__(self):
