/*
 * Wenzhao Zhang, wzhang27@ncsu.edu
 * Testing code for client API
 */

#include "client.h"

int main(int argc, char * argv[]){
	client_token *  ct=NULL;
	client_app * app=NULL;
	box * b=NULL, * g_box;
	int i, j, k, m, n, o, v=0, box_size;
	int timestep, level;
	int boxes_len;
	int timestep_len;
	int level_len;
	int dimensions;
	AMR_INT xlow_base, xhigh_base, xwidth_base, ylow_base, yhigh_base, ywidth_base, enlarge, xwidth, ywidth;
	int total_box;

	//for multi-procs, multi-put/get:
	int xglobal, yglobal, xbk, ybk, bk_num, start_bk, work_bk_len, current_bk;
	int client_procs;
	int t0, t1, pg_flag=0;
	double d1, d2;
	double put_t, get_t, max_put_t, min_put_t, total_max_put_t, total_min_put_t, max_get_t, min_get_t, total_max_get_t, total_min_get_t;

	MPI_Init(NULL, NULL);
	//MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, &i);
	ct = client_API_init();
	dimensions=2;

/*  //testing code
    if( ct->client_rank==MPI_ROOT){
		printf("MPI_THREAD_SINGLE: %d; provided: %d\n", MPI_THREAD_SINGLE, i);
        printf("MPI_THREAD_FUNNELED: %d; provided: %d\n", MPI_THREAD_FUNNELED, i);
        printf("MPI_THREAD_SERIALIZED: %d; provided: %d\n", MPI_THREAD_SERIALIZED, i);
        printf("MPI_THREAD_MULTIPLE: %d; provided: %d\n", MPI_THREAD_MULTIPLE, i);
    }
*/

	app = register_app(ct, dimensions, 6);
	if( ct->client_rank==MPI_ROOT)
		output2log("client_test.c, rank: %d, registered new app id: %d, dimension: %d, max_level: %d \n", ct->nw_rank, app->id, app->dimensions, app->max_levels);

	//single process put/get single box
/*	if( ct->client_rank==MPI_ROOT){
		timestep=0;
		level=0;

		b = allocate_box(dimensions);
		b->cood[0][0]=0;
		b->cood[0][1]=3;
		b->cood[1][0]=0;
		b->cood[1][1]=3;

		box_size = (b->cood[0][1] - b->cood[0][0]) * (b->cood[1][1] - b->cood[1][0]);
		b->data = (AMR_DATA_TYPE*)allocate_mem(sizeof(AMR_DATA_TYPE)*box_size+1, "");

		for(i=b->cood[0][0]; i<b->cood[0][1]; i++){
			for(j=b->cood[1][0]; j<b->cood[1][1]; j++){
				*(b->data + (b->cood[0][1]-b->cood[0][0])*i + j) = v++;
			}
		}

		put_box(ct, app, timestep, level, b);
		printf("client_test.c, rank: %d, pushed box to data-node: %d\n", ct->nw_rank, b->dn_rank);

		g_box = allocate_box(dimensions);
		g_box->cood = b->cood;
		xwidth = g_box->cood[0][1]-g_box->cood[0][0];
		ywidth = g_box->cood[1][1]-g_box->cood[1][0];
		if( get_box(ct, app, timestep, level, g_box)== CLIENT_API_SUCCESS ){
			printf("client_test.c, rank: %d, get box successful, app_id: %d, timestep: %d, level: %d; box data: ", ct->nw_rank, app->id, timestep, level);
			for(m=0; m<xwidth; m++){
				for(n=0; n<ywidth; n++){
					printf(" %f ", *(g_box->data + xwidth*m + n) );
				}
			}
			printf("\n");
		}else{
			printf("client_test.c, rank: %d, get box fail, app_id: %d, timestep: %d, level: %d;\n ", ct->nw_rank, app->id, timestep, level);
		}

	} //end if
*/

	/*single process put/get multiple boxes*/
/*	if( ct->client_rank==MPI_ROOT){
		boxes_len=10;
		timestep_len=4;
		level_len=2;
		enlarge=1;

		//put
		total_box=0;
		for(i=0; i< timestep_len; i++){
			for(j=level_len-1; j>=0; j--){  //enlarge coarser levels
				enlarge*=2;
				xlow_base= 0 * enlarge;
				xhigh_base= 1 * enlarge;
				xwidth_base = xhigh_base - xlow_base;
				ylow_base= 0 * enlarge;
				yhigh_base= 1 * enlarge;
				ywidth_base = yhigh_base - ylow_base;

				for(k=0; k<boxes_len; k++){  //change every box's coordinates
					v=0;
					b = allocate_box(dimensions);
					b->cood[0][0] = xlow_base + k*xwidth_base;
					b->cood[0][1] = xhigh_base + k*xwidth_base;
					b->cood[1][0] = ylow_base + k*ywidth_base;
					b->cood[1][1] = yhigh_base + k*ywidth_base;

					box_size = (b->cood[0][1] - b->cood[0][0]) * (b->cood[1][1] - b->cood[1][0]);
					b->data = (AMR_DATA_TYPE*)allocate_mem(sizeof(AMR_DATA_TYPE)*box_size+1, "");

					xwidth = b->cood[0][1]-b->cood[0][0];
					ywidth = b->cood[1][1]-b->cood[1][0];
					for(m=0; m<xwidth; m++){
						for(n=0; n<ywidth; n++){
							*(b->data + xwidth*m + n) = v++;
						}
					}

					if( put_box(ct, app, i, j, b) == CLIENT_API_SUCCESS ){
						printf("client_test.c, rank: %d, PUT box SUCCESSFUL, app_id: %d, timestep: %d, level: %d; box id: %d, [%d, %d, %d, %d] \n", ct->nw_rank, app->id, i, j, k, b->cood[0][0], b->cood[0][1], b->cood[1][0], b->cood[1][1]);
					}else{
						printf("client_test.c, rank: %d, PUT box FAIL, app_id: %d, timestep: %d, level: %d; box id: %d \n", ct->nw_rank, app->id, i, j, k);
					}

					total_box++;
				}
			}
		}  //end of put
		printf("total put box: %d\n", total_box);

		//get
		total_box=0;
		enlarge=1;
		for(i=0; i< timestep_len; i++){
			for(j=level_len-1; j>=0; j--){  //enlarge coarser levels
				enlarge*=2;
				xlow_base= 0 * enlarge;
				xhigh_base= 1 * enlarge;
				xwidth_base = xhigh_base - xlow_base;
				ylow_base= 0 * enlarge;
				yhigh_base= 1 * enlarge;
				ywidth_base = yhigh_base - ylow_base;

				for(k=0; k<boxes_len; k++){  //change every box's coordinates
					g_box = allocate_box(dimensions);
					g_box->cood[0][0] = xlow_base + k*xwidth_base;
					g_box->cood[0][1] = xhigh_base + k*xwidth_base;
					g_box->cood[1][0] = ylow_base + k*ywidth_base;
					g_box->cood[1][1] = yhigh_base + k*ywidth_base;


					if( get_box(ct, app, i, j, g_box) == CLIENT_API_SUCCESS ){
						printf("client_test.c, rank: %d, GET box SUCCESSFUL, app_id: %d, timestep: %d, level: %d; box id: %d, [%d, %d, %d, %d]\n", ct->nw_rank, app->id, i, j, k, g_box->cood[0][0], g_box->cood[0][1], g_box->cood[1][0], g_box->cood[1][1]);
					}else{
						printf("client_test.c, rank: %d, GET box FAIL, app_id: %d, timestep: %d, level: %d; box id: %d \n", ct->nw_rank, app->id, i, j, k);
					}

					total_box++;
				}
			}
		}          //end of get
		printf("total get box: %d\n", total_box);

	}// end of if
*/



	//multiple processes put/get multiple boxes; suppose the the global domain is partition by equally sized blocks in linear order
	timestep=1;
	if(argc==4){
		timestep = atoi(argv[1]);
		xglobal = yglobal = atoi(argv[2]);
		xbk=ybk = atoi(argv[3]);
	}else{
		timestep=10;
		xglobal = yglobal = 4096;
		xbk=ybk = 1024;
	}

	MPI_Comm_size(ct->client_comm, &client_procs);
	bk_num = (xglobal/xbk)*(yglobal/ybk);  //total number of blocks
	if(ct->client_rank==MPI_ROOT){
		printf("----->Multi-processes put/get %d-TS multi-boxes; For one TS, xglobal: %d, yglobal: %d, xbk: %d, ybk: %d, bk_num: %d \n", timestep, xglobal, yglobal, xbk, ybk, bk_num);
	}

	//partition box assignment
	if(client_procs==1){
		start_bk=0;
		work_bk_len=bk_num;
	}else{
		t0 = bk_num/client_procs;
		if(t0==0){ //every process cannot get at least one block
			t1 = bk_num % client_procs;
			if( ct->client_rank< t1 ){
				start_bk = ct->client_rank;
				work_bk_len = 1;
			}else{
				start_bk=-1;
				work_bk_len = 0;
			}
		}else{
			if( bk_num % client_procs ==0 ){     //every process could get evenly number of blocks
				start_bk = ct->client_rank*t0;   //starting index is 0
				work_bk_len = t0;
			}else{     //some process could get t0+1 partial blocks, but some could only get t0
				t1 = bk_num % client_procs;   //how many "left-overs"; the first t1 processes could get one more
				if(ct->client_rank<t1){
					start_bk = ct->client_rank*(t0+1);
					work_bk_len = t0+1;
				}else{
					start_bk = t1*(t0+1) + (ct->client_rank-t1)*t0 ;    //"t1*(t0+1)":  PLUS the first "t1" processes get "t0+1" blocks
					work_bk_len = t0;
				}
			}
		}
	}
	//printf("proc: %d, start_bk: %d, work_bk_len: %d \n", ct->client_rank, start_bk, work_bk_len);

	t0=t1=0;
	total_max_put_t = total_min_put_t = total_max_get_t = total_min_get_t = 0.0;
	for(i=0; i<timestep; i++){
		put_t = get_t = 0;
		for(j=0; j<work_bk_len; j++){  //put a timestep's data
			t0 = (j+start_bk) % (xglobal/xbk);  //index of the block in its current line, starting from 0
			t1 = (j+start_bk) / (yglobal/ybk);  //on which line this box is located, starting from 0

			b = allocate_box(dimensions);
			b->cood[0][0] = t0*xbk;  //xlow
			b->cood[1][0] = t1*ybk;  //ylow
			b->cood[0][1] = b->cood[0][0]+xbk;
			b->cood[1][1] = b->cood[1][0]+ybk;

			//fill in data
			v=0;
			box_size = (b->cood[0][1] - b->cood[0][0]) * (b->cood[1][1] - b->cood[1][0]);
			b->data = (AMR_DATA_TYPE*)allocate_mem(sizeof(AMR_DATA_TYPE)*box_size+1, "");
			xwidth = b->cood[0][1]-b->cood[0][0];
			ywidth = b->cood[1][1]-b->cood[1][0];
			for(m=0; m<xwidth; m++){
				for(n=0; n<ywidth; n++){
					*(b->data + xwidth*m + n) = v++;
				}
			}

			d1 = dclock();
			pg_flag = put_box(ct, app, i, 0, b);  //suppose single level
			d2 = dclock();
			put_t += (d2-d1);

			if( pg_flag == CLIENT_API_SUCCESS ){
				//printf("client_test.c, rank: %d, PUT box SUCCESSFUL, app_id: %d, timestep: %d, [%d, %d, %d, %d] \n",
				//		ct->nw_rank, -1, i, b->cood[0][0], b->cood[0][1], b->cood[1][0], b->cood[1][1]);
			}else{
				printf("client_test.c, rank: %d, PUT box FAIL, app_id: %d, timestep: %d, [%d, %d, %d, %d] \n", ct->nw_rank, app->id, i, b->cood[0][0], b->cood[0][1], b->cood[1][0], b->cood[1][1]);
			}
			destruct_box(b);
		}  //finish put a timestep's box

		MPI_Barrier(ct->client_comm);
		MPI_Reduce(&put_t, &max_put_t, 1, MPI_DOUBLE, MPI_MAX, MPI_ROOT, ct->client_comm);
		MPI_Reduce(&put_t, &min_put_t, 1, MPI_DOUBLE, MPI_MIN, MPI_ROOT, ct->client_comm);
		if(ct->client_rank==MPI_ROOT){
			total_max_put_t += max_put_t;
			total_min_put_t += min_put_t;
			printf("finished PUT boxes of timestep: %d, max time: %f, min time: %f \n", i, max_put_t, min_put_t);
		}

		//get the box's data
		for(j=0; j<work_bk_len; j++){  //get a timestep's data
			t0 = (j+start_bk) % (xglobal/xbk);  //index of the block in its current line, starting from 0
			t1 = (j+start_bk) / (yglobal/ybk);  //on which line this box is located, starting from 0

			g_box = allocate_box(dimensions);
			g_box->cood[0][0] = t0*xbk;  //xlow
			g_box->cood[1][0] = t1*ybk;  //ylow
			g_box->cood[0][1] = g_box->cood[0][0]+xbk;
			g_box->cood[1][1] = g_box->cood[1][0]+ybk;

			d1 = dclock();
			pg_flag = get_box(ct, app, i, 0, g_box);  //suppose single level
			d2 = dclock();
			get_t += (d2-d1);
			if( pg_flag == CLIENT_API_SUCCESS ){
				//printf("client_test.c, rank: %d, GET box SUCCESSFUL, app_id: %d, timestep: %d, [%d, %d, %d, %d]\n", ct->nw_rank, -1, i, g_box->cood[0][0], g_box->cood[0][1], g_box->cood[1][0], g_box->cood[1][1]);
			}else{
				printf("client_test.c, rank: %d, GET box FAIL, app_id: %d, timestep: %d, [%d, %d, %d, %d] \n", ct->nw_rank, app->id, i, g_box->cood[0][0], g_box->cood[0][1], g_box->cood[1][0], g_box->cood[1][1]);
			}
			destruct_box(g_box);
		}//finish get a timestep's box

		MPI_Barrier(ct->client_comm);
		MPI_Reduce(&get_t, &max_get_t, 1, MPI_DOUBLE, MPI_MAX, MPI_ROOT, ct->client_comm);
		MPI_Reduce(&get_t, &min_get_t, 1, MPI_DOUBLE, MPI_MIN, MPI_ROOT, ct->client_comm);
		if(ct->client_rank==MPI_ROOT){
			total_max_get_t += max_get_t;
			total_min_get_t += min_get_t;
			printf("finished GET boxes of timestep: %d, max time: %f, min time: %f \n", i, max_get_t, min_get_t);
		}

	}  //finish all timestep
	if(ct->client_rank==MPI_ROOT){  //statistics for all TS
		printf("For all timesteps(%d), total_max_PUT_t: %f,  total_min_PUT_t: %f,  total_max_GET_t: %f,  total_min_GET_t: %f \n\n",
				timestep, total_max_put_t, total_min_put_t,  total_max_get_t,  total_min_get_t);
	}

#if AMR_ENABLE_STATISTICS
	get_statistics(ct);
#endif

	MPI_Finalize();
	exit(0);
}
