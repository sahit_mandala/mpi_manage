/*
 * Wenzhao Zhang, wzhang27@ncsu.edu
 *
 * API interface for client simulations
 */
#include "client.h"

#define ONEGB_VALUES 134217728  //how many double values in 1 GB data

//default length
static int role_name_len=2;
static int role_client_len=32;
static int role_data_len=32;

#if AMR_ENABLE_STATISTICS
static double t_put_box=0;
static double t_put_box_memcpy=0;
static double t_get_box=0;
#endif

client_token * client_API_init(){
	char hostname[MPI_MAX_PROCESSOR_NAME];
	int total_comm_procs, total_grp_procs;
	int w_rank;
	int i, j;
	int role_id=ROLE_CLIENT;
	int live_flag=1; //client processes always live
	int * all_procs=NULL, * live_procs=NULL, live_count=0;
	int * all_roles=NULL; //hold the roles of all all procs, ordered by rank in the staging_comm
	//int * staging_name=NULL, staging_name_count=0; //holds global ranks of name-node processes
	//MPI_Comm nw_comm/*new world comm*/, client_comm;
	void * t_pointer=NULL;
	MPI_Group w_group;  //, nw_group/*new world group*/, client_group;
	char * all_procs_hostnames=NULL;
	int my_hostname_len=0;
	//int *client_procs, client_procs_count;
	client_token * ct=NULL;

	/*Init*/
	memset(hostname, 0, MPI_MAX_PROCESSOR_NAME);
	MPI_Get_processor_name(hostname, &i);
	MPI_Comm_size(MPI_COMM_WORLD, &total_comm_procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &w_rank);  //get world comm rank
	MPI_Comm_group(MPI_COMM_WORLD, &w_group); //get group info
	MPI_Group_size(w_group, &total_grp_procs);
#if AMR_CLIENT_DEBUG
	if(w_rank==MPI_ROOT)
		output2log("client.c; initially(root on host: %s); totally %d procs\n", hostname, total_grp_procs);
#endif
	ct = (client_token*)allocate_mem(sizeof(client_token)+1, "client_token");


	/*No real use on client side, only to cooperate with server side: find local-leader on each server node*/
	my_hostname_len = strlen(hostname);
	all_procs_hostnames = (char*)allocate_mem(sizeof(char)*MPI_MAX_PROCESSOR_NAME*total_comm_procs+1 , "all_proc_hostnames");
	MPI_Allgather(hostname, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, all_procs_hostnames, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, MPI_COMM_WORLD);
	free(all_procs_hostnames);


	/*Form new global communicator:*/
	/*to gather all global procs' live-die flag, unnecessary procs quit on server side*/
	live_flag = PROC_LIVE_FLAG;
	all_procs = (int*)allocate_mem(sizeof(int)*total_grp_procs+1, "all_procs");
	MPI_Allgather(&live_flag, 1, MPI_INT, all_procs, 1, MPI_INT, MPI_COMM_WORLD);
	for(i=0, live_count=0; i<total_grp_procs; i++ )  //count how many live procs
		if(all_procs[i] != PROC_DIE_FLAG) //if live
			live_count++;
	live_procs = (int*)allocate_mem(sizeof(int)*live_count+1, "live_procs");
	for(i=0, j=0; i< total_grp_procs; i++)  //record all live procs's ranks
		if(all_procs[i] != PROC_DIE_FLAG ) //if live
			live_procs[j++]=i;
	

	MPI_Group_incl(w_group, live_count, live_procs, & ct->nw_group);
	MPI_Comm_create(MPI_COMM_WORLD, ct->nw_group, & ct->nw_comm);
	free(live_procs);
	//MPI_Group_free(&w_group);  //might cause error if only clients procs are started


	/*In the new global comm, get all name-nodes' ranks; get all clients' ranks:*/
	MPI_Comm_size(ct->nw_comm, &total_comm_procs);
	MPI_Comm_rank(ct->nw_comm, &w_rank);

	all_roles = (int*)allocate_mem(sizeof(int)*total_comm_procs+1, "all_roles");
	MPI_Allgather(&role_id, 1, MPI_INT, all_roles, 1, MPI_INT, ct->nw_comm);
/*#if AMR_CLIENT_DEBUG
	if(w_rank==MPI_ROOT){
		output2log("client.c; all_roles: ");
		for(i=0; i< total_comm_procs; i++)
			output2log("%d, ", all_roles[i]);
		output2log("\n");
	}
#endif   */
	ct->staging_name = (int*) allocate_mem(sizeof(int) * role_name_len + 1, "staging_name");
	ct->staging_name_count = 0;
	ct->client_procs = (int*) allocate_mem(sizeof(int)*role_client_len+1, "client_procs");
	ct->client_procs_count = 0;
#if AMR_ENABLE_STATISTICS   //if there is statistics need
	ct->staging_data = (int*)allocate_mem(sizeof(int)*role_data_len+1, "");
	ct->staging_data_count=0;
#endif
	ct->nw_rank = w_rank;
	for (i = 0; i < total_comm_procs; i++) {
		if (all_roles[i] == ROLE_NAME) {
			ct->staging_name[ct->staging_name_count++] = i;
		}

		if(all_roles[i] == ROLE_CLIENT){
			ct->client_procs[ct->client_procs_count++] = i;
		}

#if AMR_ENABLE_STATISTICS
		if(all_roles[i] == ROLE_DATA || all_roles[i]==ROLE_DATA_LEADER){
			ct->staging_data[ct->staging_data_count++] = i;
		}
#endif

		if (ct->staging_name_count == role_name_len) {
			t_pointer = (int*) allocate_mem(sizeof(int) * role_name_len * 2 + 1, "t_pointer");
			memcpy(t_pointer, ct->staging_name, sizeof(int) * role_name_len);
			free(ct->staging_name);
			role_name_len *= 2;
			ct->staging_name = t_pointer;
			t_pointer = NULL;
		}

		if(ct->client_procs_count == role_client_len){
			t_pointer = (int*)allocate_mem(sizeof(int)*role_client_len*2+1, "t_pointer");
			memcpy(t_pointer, ct->client_procs, sizeof(int)*role_client_len);
			free(ct->client_procs);
			role_client_len*=2;
			ct->client_procs = t_pointer;
			t_pointer=NULL;
		}

#if AMR_ENABLE_STATISTICS
		if(ct->staging_data_count == role_data_len){
			t_pointer = (int*)allocate_mem(sizeof(int)*role_data_len*2+1, "");
			memcpy(t_pointer, ct->staging_data, sizeof(int)*role_data_len);
			free(ct->staging_data);
			role_data_len*=2;
			ct->staging_data = t_pointer;
			t_pointer=NULL;
		}
#endif
	} //end for
	MPI_Group_incl(ct->nw_group, ct->client_procs_count, ct->client_procs, & ct->client_group);
	MPI_Comm_create(ct->nw_comm, ct->client_group, &ct->client_comm);
	MPI_Comm_rank(ct->client_comm, &ct->client_rank);
	free(all_roles);
	MPI_Group_free(&w_group);

 
#endif

/*#if AMR_SERVER_DEBUG
 	if(w_rank==MPI_ROOT){
		output2log("client.c; proc(w_rank:%d, on host: %s), all known(%d) name-nodes w_ranks: ", w_rank, hostname, ct->staging_name_count);
		for(i=0; i<ct->staging_name_count; i++)
			output2log("%d, ", ct->staging_name[i]);

#if AMR_ENABLE_STATISTICS
		output2log(";   all known data-nodes(%d): ", ct->staging_data_count);
		for(i=0; i<ct->staging_data_count; i++)
			output2log("%d, ", ct->staging_data[i]);
#endif

		output2log("\n");
	}
#endif  */

	if(ct->staging_name_count==0){  //if no name node
		output2log("client.c; NO name-node available, quit....\n");
		MPI_Finalize();
		exit(0);
	}  /*   */

	srand( time(NULL) );  //set random seed

	return ct;
}

/*
 * client root send request to the first name-node, then scatter the result id to other client.
 */
client_app * register_app(client_token * ct, int dimension, int max_levels){
	int * data=NULL;
	int app_id=0;
	client_app * ca=NULL;

	if(ct==NULL || dimension<=1 || max_levels<1){
#if AMR_CLIENT_DEBUG
		output2log("client.c, register_app; client_token cannot be NULL; dimension must be at least 2; max_levels must be at least 1.\n");
#endif
		return NULL;
	}

	if( ct->client_rank==MPI_ROOT){  //root client
		data = allocate_mem(sizeof(int)*3+1, "register_app, data");
		data[0] = M_REG_APP_ID;
		data[1] = dimension;
		data[2] = max_levels;

		MPI_Send(data, 3, MPI_INT, ct->staging_name[0], M_REG_APP_ID, ct->nw_comm);  //send request
		MPI_Recv(&app_id, 1, MPI_INT, ct->staging_name[0], M_REG_APP_ID, ct->nw_comm, MPI_STATUS_IGNORE);  //receive APP id as an int
	}
	MPI_Barrier(ct->client_comm);
	MPI_Bcast(&app_id, 1, MPI_INT, MPI_ROOT, ct->client_comm);

	ca = (client_app*)allocate_mem(sizeof(client_app)+1, "client.c, register_app;");
	ca->id = app_id;
	ca->dimensions=dimension;
	ca->max_levels=max_levels;

	free(data);
	sleep(2);   //<----  ????
	return ca;
}

int put_box(client_token * ct, client_app * app, int timestep, int level, box * b){
	int nn_rank;  //name-node rank to send the reg request
	int dn_rank;  //data-node rank to push the actual box
	int reg_data_len=0, push_data_fix_len=0; //, push_data_len=0;
	AMR_INT box_key, box_size;
	AMR_INT * reg_data=NULL;
	void * push_data=NULL;
	AMR_INT * push_data_int=NULL;
	//MPI_Request req;
	//void * bak_data=NULL;  //


#if AMR_ENABLE_STATISTICS
	double t_start, t_end;
#endif

	if(ct==NULL || app==NULL || timestep<0 || level<0 || b==NULL || b->data==NULL){
/*
#if AMR_CLIENT_DEBUG
		output2log("ERROR: client.c; push box, invalid arg: %d, %d, %d, %d, %d, %d\n", ct, app, timestep, level, b, b->data);
#endif
*/
		return 1;
	}

	if(app->dimensions!=b->dimensions  ||  level > app->max_levels || level<0){
#if AMR_CLIENT_DEBUG
		output2log("ERROR: client.c; push box, inconsistent dim size(%d != %d); or given level(%d) exceeds max value(%d)\n", app->dimensions, b->dimensions, level, app->max_levels);
#endif
		return 1;
	}

	nn_rank = ct->staging_name[ timestep%ct->staging_name_count ]; //if multiple name-nodes, each timestep is distributes to all namenodes in round-robin manner

	//prepare registry message:
	reg_data_len = M_REG_PUSH_BOX_FIX_PART+app->dimensions*2;
	reg_data = (AMR_INT*)allocate_mem(sizeof(AMR_INT)*reg_data_len+1, "");
	reg_data[0]=M_REG_PUSH_BOX;
	reg_data[1]=app->id;
	reg_data[2]=timestep;
	reg_data[3]=level;
	switch(b->dimensions){
	case AMR_2D:
		reg_data[M_REG_PUSH_BOX_FIX_PART] = b->cood[0][0];
		reg_data[M_REG_PUSH_BOX_FIX_PART+1] = b->cood[0][1];
		reg_data[M_REG_PUSH_BOX_FIX_PART+2] = b->cood[1][0];
		reg_data[M_REG_PUSH_BOX_FIX_PART+3] = b->cood[1][1];
		box_key = b->cood[0][0] + b->cood[0][1] + b->cood[1][0] + b->cood[1][1];
		box_size = (b->cood[0][1]-b->cood[0][0]) * (b->cood[1][1]-b->cood[1][0]);
		break;
	case AMR_3D:
		reg_data[M_REG_PUSH_BOX_FIX_PART] = b->cood[0][0];
		reg_data[M_REG_PUSH_BOX_FIX_PART+1] = b->cood[0][1];
		reg_data[M_REG_PUSH_BOX_FIX_PART+2] = b->cood[1][0];
		reg_data[M_REG_PUSH_BOX_FIX_PART+3] = b->cood[1][1];
		reg_data[M_REG_PUSH_BOX_FIX_PART+4] = b->cood[2][0];
		reg_data[M_REG_PUSH_BOX_FIX_PART+5] = b->cood[2][1];
		box_key = b->cood[0][0] + b->cood[0][1] + b->cood[1][0] + b->cood[1][1] + b->cood[2][0] + b->cood[2][1];
		box_size = (b->cood[0][1]-b->cood[0][0]) * (b->cood[1][1]-b->cood[1][0]) * (b->cood[2][1]-b->cood[2][0]);
		break;
	default:
		free(reg_data);
		return 1;
	}
	reg_data[4]=box_key;
	reg_data[5]=box_size;

	//send reg-message to NN
	MPI_Send(reg_data, reg_data_len, MPI_INT, nn_rank, M_REG_PUSH_BOX, ct->nw_comm);  //send request
	MPI_Recv(&dn_rank, 1, MPI_INT, nn_rank, M_REG_PUSH_BOX, ct->nw_comm, MPI_STATUS_IGNORE);  //get DN rank to which box's data will be sent

	if(dn_rank<0){
#if AMR_CLIENT_DEBUG
		output2log("ERROR: client.c; push box, reg-push failed\n");
#endif
		free(reg_data);
		return 1;
	}
	b->dn_rank = dn_rank;


	//prepare to push actual data
#if AMR_ENABLE_STATISTICS
	t_start = dclock();
#endif
	push_data_fix_len = M_PUSH_BOX_FIX_PART+app->dimensions*2;  //how many ints
	push_data = allocate_mem( push_data_fix_len*sizeof(AMR_INT)+1, "" );  //only metadata
	push_data_int = (int*)push_data;

#if AMR_ENABLE_STATISTICS
	t_end = dclock();
	t_put_box_memcpy += (t_end - t_start);
#endif

	//prepare metadata
	push_data_int[0] = M_PUSH_BOX;
	push_data_int[1] = app->id;
	push_data_int[2] = timestep;
	push_data_int[3] = level;
	push_data_int[4] = box_key;
	push_data_int[5] = box_size;n
	switch(b->dimensions){
	case AMR_2D:
		push_data_int[M_PUSH_BOX_FIX_PART] = b->cood[0][0];
		push_data_int[M_PUSH_BOX_FIX_PART+1] = b->cood[0][1];
		push_data_int[M_PUSH_BOX_FIX_PART+2] = b->cood[1][0];
		push_data_int[M_PUSH_BOX_FIX_PART+3] = b->cood[1][1];
		break;
	case AMR_3D:
		push_data_int[M_PUSH_BOX_FIX_PART] = b->cood[0][0];
		push_data_int[M_PUSH_BOX_FIX_PART+1] = b->cood[0][1];
		push_data_int[M_PUSH_BOX_FIX_PART+2] = b->cood[1][0];
		push_data_int[M_PUSH_BOX_FIX_PART+3] = b->cood[1][1];
		push_data_int[M_PUSH_BOX_FIX_PART+4] = b->cood[2][0];
		push_data_int[M_PUSH_BOX_FIX_PART+5] = b->cood[2][1];
		break;
	}

#if AMR_ENABLE_STATISTICS
	t_start = dclock();
#endif
	MPI_Send(push_data, push_data_fix_len, MPI_INT, dn_rank, M_PUSH_BOX, ct->nw_comm);  //, &req); //send metadata to DN
	MPI_Send(b->data, box_size, AMR_MPI_DATA_TYPE, dn_rank, M_PUSH_BOX_DATA, ct->nw_comm);   //send actual binary data DN


	free(reg_data);
	//MPI_Wait(&req, MPI_STATUSES_IGNORE);
	free(push_data);

#if AMR_ENABLE_STATISTICS
	t_end = dclock();
	t_put_box += (t_end - t_start);
#endif

	return 0;
}

int get_box(client_token * ct, client_app * app, int timestep, int level, box * b){
	int nn_rank;  //name-node rank to send the reg request
	int dn_rank;  //data-node rank to push the actual box
	int req_data_len=0, push_data_len=0;
	AMR_INT box_key, box_size;
	AMR_INT * req_data=NULL;
	MPI_Status status;
	int msg_size;
	//MPI_Request req;

#if AMR_ENABLE_STATISTICS
	double t_start, t_end;
#endif

	if(ct==NULL || app==NULL || timestep<0 || level<0 || b==NULL){
#if AMR_CLIENT_DEBUG
		output2log("ERROR: client.c; get box, invalid arg: %d, %d, %d, %d, %d\n", ct, app, timestep, level, b);
#endif
		return 1;
	}

	if(level > app->max_levels || level<0){
#if AMR_CLIENT_DEBUG
		output2log("ERROR: client.c; get box, given level(%d) exceeds max value(%d) or cannot be less than 0\n", b->dimensions, level, app->max_levels);
#endif
		return 1;
	}

	//prepare message:
	req_data_len = M_GET_BOX_FIX_PART+app->dimensions*2;
	req_data = (AMR_INT*)allocate_mem(sizeof(AMR_INT)*req_data_len+1, "");
	req_data[0]=M_GET_BOX;
	req_data[1]=app->id;
	req_data[2]=timestep;
	req_data[3]=level;
	switch(b->dimensions){
	case AMR_2D:
		req_data[M_GET_BOX_FIX_PART] = b->cood[0][0];
		req_data[M_GET_BOX_FIX_PART+1] = b->cood[0][1];
		req_data[M_GET_BOX_FIX_PART+2] = b->cood[1][0];
		req_data[M_GET_BOX_FIX_PART+3] = b->cood[1][1];
		box_key = b->cood[0][0] + b->cood[0][1] + b->cood[1][0] + b->cood[1][1];
		box_size = (b->cood[0][1]-b->cood[0][0]) * (b->cood[1][1]-b->cood[1][0]);
		break;
	case AMR_3D:
		req_data[M_GET_BOX_FIX_PART] = b->cood[0][0];
		req_data[M_GET_BOX_FIX_PART+1] = b->cood[0][1];
		req_data[M_GET_BOX_FIX_PART+2] = b->cood[1][0];
		req_data[M_GET_BOX_FIX_PART+3] = b->cood[1][1];
		req_data[M_GET_BOX_FIX_PART+4] = b->cood[2][0];
		req_data[M_GET_BOX_FIX_PART+5] = b->cood[2][1];
		box_key = b->cood[0][0] + b->cood[0][1] + b->cood[1][0] + b->cood[1][1] + b->cood[2][0] + b->cood[2][1];
		box_size = (b->cood[0][1]-b->cood[0][0]) * (b->cood[1][1]-b->cood[1][0]) * (b->cood[2][1]-b->cood[2][0]);
		break;
	default:
		free(req_data);
		return 1;
	}
	req_data[4]=box_key;
	req_data[5]=box_size;

	//send request msg
	if(b->dn_rank==-1){  //need to request the name node
		nn_rank = ct->staging_name[ timestep%ct->staging_name_count ]; //if multiple name-nodes, each timestep is distributes to all namenodes in round-robin manner
		MPI_Send(req_data, req_data_len, MPI_INT, nn_rank, M_GET_BOX, ct->nw_comm); //, &req);   //, MPI_REQUEST_NULL);  //send request
	}else{  //call the specific data node directly
		MPI_Send(req_data, req_data_len, MPI_INT, b->dn_rank, M_GET_BOX, ct->nw_comm);  //, &req);   //, MPI_REQUEST_NULL);
	}

#if AMR_ENABLE_STATISTICS
	t_start = dclock();
#endif
	//to receive the box's data
	b->data = (AMR_DATA_TYPE*)allocate_mem(sizeof(AMR_DATA_TYPE)*box_size+1, "");
	MPI_Recv(b->data, box_size, AMR_MPI_DATA_TYPE, MPI_ANY_SOURCE, M_GET_BOX, ct->nw_comm, &status);

	//check if possible error
	MPI_Get_count(&status, AMR_MPI_DATA_TYPE, &msg_size);
	if(msg_size==1 && box_size!=1 && b->data[0]==-1.0){  //suppose box size should be larger than 1
		free(b->data);
		b->data=NULL;
#if AMR_CLIENT_DEBUG
		output2log("ERROR: client.c; get_box, failed to get the actual data, box_key: %d, box_size: %d\n", box_key, box_size);
#endif

		free(req_data);
		return 1;
	}

	free(req_data);

#if AMR_ENABLE_STATISTICS
	t_end = dclock();
	t_get_box += (t_end - t_start);
#endif
	return 0;
}

#if AMR_ENABLE_STATISTICS
void get_statistics(client_token * ct){
	int i, sdata;
	double *rdata;
	double t_put_box_workload;  //put_box
	double t_put_box_search_ts;
	double t_put_box_search_create_ts;
	double t_put_box_search_level;
	double t_put_box_search_create_level;
	double t_put_box_insert_ht;
	double t_get_box_search;   //get_box
	double t_get_box_search_ht;
	double t_get_box_send;
	double t_put_total;
	double t_get_total;
	double t_process_req;
	double stat_server_recv;
	unsigned long s_workload;

	double t_max_client_put,  t_max_client_put_memcpy, t_max_client_get;


	if(ct==NULL ){
#if AMR_CLIENT_DEBUG
		output2log("ERROR: client.c; get_statistics, invalid arg: %d \n", ct );
#endif
		return;
	}

	MPI_Reduce(&t_put_box, &t_max_client_put, 1, MPI_DOUBLE, MPI_MAX, MPI_ROOT, ct->client_comm);
	MPI_Reduce(&t_put_box_memcpy, &t_max_client_put_memcpy, 1, MPI_DOUBLE, MPI_MAX, MPI_ROOT, ct->client_comm);
	MPI_Reduce(&t_get_box, &t_max_client_get, 1, MPI_DOUBLE, MPI_MAX, MPI_ROOT, ct->client_comm);

	if(ct->client_rank!=MPI_ROOT){  //only client root proceeds to get server statistics
		return;
	}

	output2log("client.c, get_statistics, CLIENT-Side, max_put: %f (%e),  max_put_memcpy: %f (%e),  max_get: %f (%e) \n",
			t_max_client_put, t_max_client_put,   t_max_client_put_memcpy, t_max_client_put_memcpy,   t_max_client_get, t_max_client_get);

	t_put_box_workload = t_put_box_search_ts = t_put_box_search_create_ts = t_put_box_search_level =
			t_put_box_search_create_level = t_put_box_insert_ht = t_get_box_search = t_get_box_search_ht =
					t_get_box_send = t_put_total = t_get_total = t_process_req = stat_server_recv = 0.0;
	s_workload=0;


	sdata = M_GET_STATISTICS;
	rdata = (double*)allocate_mem( (2*M_GET_STATISTICS_NN_FIX_DOUBLE+1)*sizeof(double)+1, "");
	for(i=0; i<ct->staging_name_count; i++){
		MPI_Send(&sdata, 1, MPI_INT, ct->staging_name[i], M_GET_STATISTICS, ct->nw_comm);

		MPI_Recv(rdata, 2*M_GET_STATISTICS_NN_FIX_DOUBLE+1, MPI_DOUBLE, ct->staging_name[i], M_GET_STATISTICS, ct->nw_comm, MPI_STATUS_IGNORE);

		output2log("client.c, get_statistics, Name-node(%d) statistics: \n", ct->staging_name[i]);
		output2log("		t_put_box_workload;  max: %f,  min: %f \n", rdata[0], rdata[1]);
		output2log("		t_put_box_search_ts;  max: %f,  min: %f \n", rdata[2], rdata[3]);
		output2log("		t_put_box_search_create_ts;  max: %f,  min: %f \n", rdata[4], rdata[5]);
		output2log("		t_put_box_search_level;  max: %f,  min: %f \n", rdata[6], rdata[7]);
		output2log("		t_put_box_search_create_level;  max: %f,  min: %f \n", rdata[8], rdata[9]);
		output2log("		t_put_box_insert_ht;  max: %f,  min: %f \n", rdata[10], rdata[11]);
		output2log("		t_get_box_search;  max: %f,  min: %f \n", rdata[12], rdata[13]);
		output2log("		t_get_box_search_ht;  max: %f,  min: %f \n", rdata[14], rdata[15]);

		output2log("		t_put_total;  max: %f,  min: %f \n", rdata[16], rdata[17]);
		output2log("		t_get_total;  max: %f,  min: %f \n", rdata[18], rdata[19]);
		output2log("		t_process_req;  max: %f,  min: %f \n", rdata[20], rdata[21]);
		output2log("		stat_server_recv: %f \n", rdata[22] );
		//memset(rdata, 0, 2*M_GET_STATISTICS_NN_FIX_DOUBLE*sizeof(double));
	}
	free(rdata);


	rdata = (double*)allocate_mem(M_GET_STATISTICS_DN_FIX_DOUBLE*sizeof(double)+1, "");
	for(i=0; i<ct->staging_data_count; i++){
		MPI_Send(&sdata, 1, MPI_INT, ct->staging_data[i], M_GET_STATISTICS, ct->nw_comm);

		MPI_Recv(rdata, M_GET_STATISTICS_DN_FIX_DOUBLE, MPI_DOUBLE, ct->staging_data[i], M_GET_STATISTICS, ct->nw_comm, MPI_STATUS_IGNORE);

/*
		output2log("client.c, get_statistics, Data-node(%d) statistics: \n", ct->staging_data[i]);
		output2log("		t_put_box_search_ts: %f (%e) \n", rdata[0], rdata[0]);
		output2log("		t_put_box_search_create_ts: %f (%e) \n", rdata[1], rdata[1]);
		output2log("		t_put_box_search_level: %f (%e) \n", rdata[2], rdata[2]);
		output2log("		t_put_box_search_create_level: %f (%e) \n", rdata[3], rdata[3]);
		output2log("		t_put_box_insert_ht: %f (%e) \n", rdata[4], rdata[4]);
		output2log("		t_get_box_search: %f (%e) \n", rdata[5], rdata[5]);
		output2log("		t_get_box_search_ht: %f (%e) \n", rdata[6], rdata[6]);
		output2log("		t_get_box_send: %f (%e) \n", rdata[7], rdata[7]);
		output2log("		t_put_total: %f (%e) \n", rdata[8], rdata[8]);
		output2log("		t_get_total: %f (%e) \n", rdata[9], rdata[9]);
		output2log("		s_workload: %llu,  %f GB \n", (unsigned long)rdata[10], ((unsigned long)rdata[10])*1.0/ONEGB_VALUES);
		output2log("		stat_server_recv: %f (%e) \n", rdata[11], rdata[11]);
*/

		t_put_box_search_ts += rdata[0];
		t_put_box_search_create_ts += rdata[1];
		t_put_box_search_level += rdata[2];
		t_put_box_search_create_level += rdata[3];
		t_put_box_insert_ht += rdata[4];
		t_get_box_search += rdata[5];
		t_get_box_search_ht += rdata[6];
		t_get_box_send += rdata[7];
		t_put_total += rdata[8];
		t_get_total += rdata[9];
		s_workload += rdata[10];
		stat_server_recv += rdata[11];

		//memset(rdata, 0, 2*M_GET_STATISTICS_DN_FIX_DOUBLE*sizeof(double));
	}
	output2log("client.c, get_statistics, ALL Data-nodes statistics: \n");
	output2log("		t_put_box_search_ts: %f (%e) \n", t_put_box_search_ts, t_put_box_search_ts);
	output2log("		t_put_box_search_create_ts: %f (%e) \n", t_put_box_search_create_ts, t_put_box_search_create_ts);
	output2log("		t_put_box_search_level: %f (%e) \n", t_put_box_search_level, t_put_box_search_level);
	output2log("		t_put_box_search_create_level: %f (%e) \n", t_put_box_search_create_level, t_put_box_search_create_level);
	output2log("		t_put_box_insert_ht: %f (%e) \n", t_put_box_insert_ht, t_put_box_insert_ht);
	output2log("		t_get_box_search: %f (%e) \n", t_get_box_search, t_get_box_search);
	output2log("		t_get_box_search_ht: %f (%e) \n", t_get_box_search_ht, t_get_box_search_ht);
	output2log("		t_get_box_send: %f (%e) \n", t_get_box_send, t_get_box_send);
	output2log("		t_put_total: %f (%e) \n", t_put_total, t_put_total);
	output2log("		t_get_total: %f (%e) \n", t_get_total, t_get_total);
	output2log("		s_workload: %llu,  %f GB \n", s_workload, (s_workload*1.0)/ONEGB_VALUES);
	output2log("		stat_server_recv: %f (%e) \n", stat_server_recv, stat_server_recv);
	free(rdata);

}
#endif
