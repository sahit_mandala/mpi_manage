/*
 * Wenzhao Zhang, wzhang27@ncsu.edu
 */

//#include <stdlib.h>
#include "util.h"
#include <pthread.h>


/* ----------start define hash-table structs---------- */
typedef struct _ht_list_concurrent_st{
	int elem_number; /* number of elements in the list */
	_ht_list_elem * l_head;
	_ht_list_elem * l_tail;

	pthread_rwlock_t ht_list_lock;  //used when want to insert a box into the list
}_ht_list_concurrent;

typedef struct _hash_table_concurrent_st {
	_ht_list_concurrent ** array;
	int size; /* array's length, not actual number of elements in the hash table.  tiny, small, medium, large */

	//Used when want to insert new element(_ht_list) to an empty cell of the HT.
	//Because the HT might be very large, might reduce performance if creating a lock for every cell. A compromise is to cut the HT to partitions, each one is protected by a lock
	pthread_rwlock_t * ht_locks;

	int partition_size;
} _hash_table_concurrent;
/* ----------end define hash-table structs---------- */



/* ----------start define Concurrent hash-table functions---------- */
/* reference: http://blog.csdn.net/gdujian0119/article/details/6777239
 * http://www.cse.yorku.ca/~oz/hash.html */
static _ht_list_concurrent * create_htlist(){
	_ht_list_concurrent * l_t = NULL;

	l_t = (_ht_list_concurrent*)allocate_mem(sizeof(_ht_list_concurrent)+1, "");

	l_t->elem_number = 0;
	l_t->l_head = NULL;
	l_t->l_tail = NULL;
	pthread_rwlock_init(&l_t->ht_list_lock, NULL);

	return l_t;
}

/*
 * key: hash key
 * data: the actual data of the box
 */
static _ht_list_elem * create_htlist_elem(int key, void * data){
	_ht_list_elem * le_t = NULL;

	if(data==NULL){
#if AMR_UTIL_DEBUG
		printf("create_htlist_elem, key or data is NULL\n");
#endif
		return NULL;
	}

	le_t = (_ht_list_elem*)allocate_mem( sizeof(_ht_list_elem)+1, "" );

	le_t->key = key;
	le_t->data = data;
	le_t->next = NULL;

	return le_t;
}

static int en_htlist(_ht_list_elem * e_t, _ht_list_concurrent * l_t){  //insert element to list
	if(e_t==NULL || l_t==NULL){
#if AMR_UTIL_DEBUG
		printf("en_htlist, list_elem or list is NULL.\n");
#endif
		return 1;
	}

	pthread_rwlock_wrlock(&l_t->ht_list_lock);
    if(l_t->elem_number==0){ /* if list empty */
    	l_t->l_head = l_t->l_tail = e_t;
    }else{
    	l_t->l_tail->next = e_t;
    	l_t->l_tail = e_t;
    }
    l_t->elem_number++;
    pthread_rwlock_unlock(&l_t->ht_list_lock);

    return 0;
}

/*
 * only remove the element-struct from the list, not free the actual data
 */
static int del_htlist_elem(AMR_INT dimensions, AMR_INT ** cood, _ht_list_concurrent * l){
	_ht_list_elem * lp1=NULL, * lp2=NULL, * to_del=NULL;
	int i, size, found_flag=0;
	box * b=NULL;

	if(dimensions<=1 || cood==NULL || l==NULL){
#if AMR_UTIL_DEBUG
		printf("del_htlist_elem, invalid args\n");
#endif
		return 1;
	}

    size = l->elem_number;
    lp1 = l->l_head;
    for(i=0; i< size; i++){
    	b = (box*)lp1->data;
		switch(dimensions){
		case AMR_2D:
			if( b->cood[0][0]==cood[0][0] && b->cood[0][1]==cood[0][1] && b->cood[1][0]==cood[1][0] && b->cood[1][1]==cood[1][1] )
				found_flag=1;
			break;
		case AMR_3D:
			if( b->cood[0][0]==cood[0][0] && b->cood[0][1]==cood[0][1] && b->cood[1][0]==cood[1][0] && b->cood[1][1]==cood[1][1] && b->cood[2][0]==cood[2][0] && b->cood[2][1]==cood[2][1])
				found_flag=1;
			break;
		}

		pthread_rwlock_wrlock(&l->ht_list_lock);
    	if( found_flag ){ /* if found */
    		to_del=lp1;
    		if(lp1==l->l_head){ /* if to_del is the head */
    			if(l->elem_number==1){ /* if only one elem in the list */
    				l->l_head = l->l_tail = NULL;
    			}else{
    				l->l_head = l->l_head->next;
    			}
    			break;
    		} else if(lp1==l->l_tail){
    			l->l_tail = lp2;
    			lp2->next = NULL;
    			break;
    		}else{
    			lp2->next = lp1->next;
    			break;
    		}
    	}
    	pthread_rwlock_unlock(&l->ht_list_lock);

    	lp2=lp1;
    	lp1 = lp1->next;
    } /* end for */

    if(to_del!=NULL){
    	to_del->next = NULL; /* cut relation with the list */
    	l->elem_number--;
    }

    return to_del!=NULL ? 0 : 1;
}

/*Concurrent HashTable public interfaces:*/
box_hash_table ht_create_concurrent(int size, int partition_size){
	_hash_table_concurrent * ht = NULL;
	int i;

	if(size<=0 || partition_size<=0){
#if AMR_UTIL_DEBUG
		printf("ht_create, size<=0, or partition_size<=0.\n");
#endif
		return NULL;
	}

	ht = (_hash_table_concurrent*)allocate_mem(sizeof(_hash_table_concurrent)+1, "");

	ht->array = (_ht_list_concurrent**)allocate_mem( sizeof(_ht_list_concurrent*)*size + 1, "" );
	ht->size = size;
	ht->partition_size = partition_size;
	ht->ht_locks = (pthread_rwlock_t*)allocate_mem( sizeof(pthread_rwlock_t)*partition_size+1, "");
	for(i=0; i<partition_size; i++)
		pthread_rwlock_init(& (ht->ht_locks[i]), NULL);

	return (box_hash_table)ht;
}

/*
 * key: the sum of a box's coordinates
 */
int ht_insert_key_concurrent(box * b, box_hash_table t, AMR_INT key, uint32_t seed){
	int ht_index=0, partition;
	_hash_table_concurrent * ht = NULL;

	if(b==NULL || t==NULL){
#if AMR_UTIL_DEBUG
		printf("ht_insert, b(%d) or t(%d) is NULL.\n", b, t);
#endif
		return 1;
	}

	ht = (_hash_table_concurrent*)t;
	ht_index = fasthash32(&key, HASH_KEY_LEN, seed) % ht->size;
	partition = ht_index%ht->partition_size;

	//printf("util.c, ht_insert_key, key: %d, ht_index: %d, partition: %d\n", key, ht_index, partition);
	if(ht->array[ht_index]==NULL){
		pthread_rwlock_wrlock( & (ht->ht_locks[partition]) );
		if(ht->array[ht_index]==NULL)
			ht->array[ht_index] = create_htlist();
		pthread_rwlock_unlock( & (ht->ht_locks[partition]) );
	}

	en_htlist( create_htlist_elem(key, b), ht->array[ht_index] );

	return 0;
}
/*
 * b: the actual pointer of the box struct
 */
int ht_insert_concurrent(box * b, box_hash_table t, uint32_t seed){
	int ht_index=0, partition;
	AMR_INT key;
	_hash_table_concurrent * ht = NULL;

	if(b==NULL || t==NULL){
#if AMR_UTIL_DEBUG
		printf("ht_insert, b(%d) or t(%d) is NULL.\n", b, t);
#endif
		return 1;
	}

	switch(b->dimensions){
	case AMR_2D:
		key = b->cood[0][0] + b->cood[0][1] + b->cood[1][0] + b->cood[1][1];
		break;
	case AMR_3D:
		key = b->cood[0][0] + b->cood[0][1] + b->cood[1][0] + b->cood[1][1] + b->cood[2][0] + b->cood[2][1];
		break;
	default:
		return 1;
	}
	ht = (_hash_table_concurrent*)t;
	ht_index = fasthash32(&key, HASH_KEY_LEN, seed) % ht->size;  //_SEED
	partition = ht_index%ht->partition_size;

	if(ht->array[ht_index]==NULL){
		pthread_rwlock_wrlock( & (ht->ht_locks[partition]) );
		if(ht->array[ht_index]==NULL)
			ht->array[ht_index] = create_htlist();
		pthread_rwlock_unlock( & (ht->ht_locks[partition]) );
	}

	en_htlist( create_htlist_elem(key, b), ht->array[ht_index] );

	return 0;
}

//no locks for ht_search ??
box * ht_search_key_concurrent(AMR_INT dimensions, AMR_INT ** cood, box_hash_table t, AMR_INT key, uint32_t seed){
	int ht_index=0;
	_hash_table_concurrent * ht = NULL;
	_ht_list_elem * ht_le = NULL;
	box * b=NULL;

	if(dimensions<=1 || cood==NULL || t==NULL){
#if AMR_UTIL_DEBUG
		printf("ht_search, invalid args, dimensions(%d), cood(%d), t(%d)\n", dimensions, cood, t);
#endif
		return NULL;
	}

	ht = (_hash_table_concurrent*)t;
	ht_index = fasthash32(&key, HASH_KEY_LEN, seed) % ht->size;

	if(ht->array[ht_index]==NULL){
		return NULL;
	}

	ht_le = ht->array[ht_index]->l_head;
	while(ht_le!=NULL){
		if( key == ht_le->key ){
			b = (box*)ht_le->data;
			switch(dimensions){
			case AMR_2D:
				if( b->cood[0][0]==cood[0][0] && b->cood[0][1]==cood[0][1] && b->cood[1][0]==cood[1][0] && b->cood[1][1]==cood[1][1] )
					return b;
				break;
			case AMR_3D:
				if( b->cood[0][0]==cood[0][0] && b->cood[0][1]==cood[0][1] && b->cood[1][0]==cood[1][0] && b->cood[1][1]==cood[1][1] && b->cood[2][0]==cood[2][0] && b->cood[2][1]==cood[2][1])
					return b;
				break;
			}
		}
		ht_le = ht_le->next;
	}

	return NULL;

}
box * ht_search_concurrent(AMR_INT dimensions, AMR_INT ** cood, box_hash_table t, uint32_t seed){   //no list lock??
	int ht_index=0;
	_hash_table_concurrent * ht = NULL;
	_ht_list_elem * ht_le = NULL;
	AMR_INT key;
	box * b=NULL;

	if(dimensions<=1 || cood==NULL || t==NULL){
#if AMR_UTIL_DEBUG
		printf("ht_search, invalid args, dimensions(%d), cood(%d), t(%d)\n", dimensions, cood, t);
#endif
		return NULL;
	}

	switch(dimensions){
	case AMR_2D:
		key = cood[0][0] + cood[0][1] + cood[1][0] + cood[1][1];
		break;
	case AMR_3D:
		key = cood[0][0] + cood[0][1] + cood[1][0] + cood[1][1] + cood[2][0] + cood[2][1];
		break;
	default:
		return NULL;
	}

	ht = (_hash_table_concurrent*)t;
	ht_index = fasthash32(&key, HASH_KEY_LEN, seed) % ht->size;

	if(ht->array[ht_index]==NULL){
		return NULL;
	}

	ht_le = ht->array[ht_index]->l_head;
	while(ht_le!=NULL){
		if( key == ht_le->key ){
			b = (box*)ht_le->data;
			switch(dimensions){
			case AMR_2D:
				if( b->cood[0][0]==cood[0][0] && b->cood[0][1]==cood[0][1] && b->cood[1][0]==cood[1][0] && b->cood[1][1]==cood[1][1] )
					return b;
				break;
			case AMR_3D:
				if( b->cood[0][0]==cood[0][0] && b->cood[0][1]==cood[0][1] && b->cood[1][0]==cood[1][0] && b->cood[1][1]==cood[1][1] && b->cood[2][0]==cood[2][0] && b->cood[2][1]==cood[2][1])
					return b;
				break;
			}
		}
		ht_le = ht_le->next;
	}

	return NULL;
}

int ht_delete_concurrent(AMR_INT dimensions, AMR_INT ** cood, box_hash_table t){
	_ht_list_elem * le = NULL;
	int ht_index=0;
	_hash_table_concurrent * ht = NULL;
	_ht_list_elem * ht_le = NULL;
	AMR_INT key;

	if(dimensions<=1 || cood==NULL || t==NULL){
#if AMR_UTIL_DEBUG
		printf("ht_search, invalid args.\n");
#endif
		return NULL;
	}

	switch(dimensions){
	case AMR_2D:
		key = cood[0][0] + cood[0][1] + cood[1][0] + cood[1][1];
		break;
	case AMR_3D:
		key = cood[0][0] + cood[0][1] + cood[1][0] + cood[1][1] + cood[2][0] + cood[2][1];
		break;
	default:
		return NULL;
	}

	ht = (_hash_table_concurrent*)t;
	ht_index = fasthash32(&key, HASH_KEY_LEN, cood[1][1]) % ht->size;

	if(ht->array[ht_index]==NULL)
		return 1;

	return del_htlist_elem(dimensions, cood, ht->array[ht_index]);
}

void ht_destruct_concurrent(box_hash_table h){
	_hash_table_concurrent * ht = NULL;
	_ht_list_elem * le1 = NULL, * le2=NULL;
	int i=0, s=0;
	if(h==NULL)
		return;

	ht = (_hash_table_concurrent*)h;
	s = ht->size;
	for(i=0;i<s;i++){
		if(ht->array[i]==NULL)
			continue;

		le1 = ht->array[i]->l_head;
		while (le1 != NULL) { /* iterate through an element of the ht, */
			le2 = le1;
			le1 = le1->next;

			le2->data = NULL;
			le2->key = NULL;
			le2->next = NULL;
			free(le2);  /* free list element(_symbol) */
		}
		free(ht->array[i]); /* free list itself */
		ht->array[i]=NULL;
	}

	for(i=0; i<ht->partition_size; i++)
		pthread_rwlock_destroy(& (ht->ht_locks[i]) );

	free(ht);
}
/* ----------end define concurrent hash-table functions---------- */

