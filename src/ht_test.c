/*
 * box hash table test
 */
#include "util.h"

int main(int argc, char * argv[]){
	box_hash_table ht=NULL;
	box * b1=NULL;
	box * b2=NULL;
	box * b3=NULL;
	box * t=NULL;

	ht = ht_create(HT_ARRAY_SIZE_T);
	printf("sizeof(box): %d\n", sizeof(box));
	b1 = (box*)allocate_mem(sizeof(box)+1, "");
	b2 = (box*)allocate_mem(sizeof(box)+1, "");
	b3 = (box*)allocate_mem(sizeof(box)+1, "");
	printf("b1: %d, b2: %d, b3: %d\n", b1, b2, b3);


	b1->cood=(AMR_INT **)allocate_coodinates(2);
	//printf("%d %d %d %d\n", b1->cood[0][0], b1->cood[0][1], b1->cood[1][0], b1->cood[1][1]);
	b2->cood=(AMR_INT **)allocate_coodinates(2);
	b3->cood=(AMR_INT **)allocate_coodinates(2);

	b1->dimensions=2;
	b1->cood[0][0]=0;
	b1->cood[0][1]=1;
	b1->cood[1][0]=0;
	b1->cood[1][1]=1;
	t=b1;
	//printf("%d %d %d %d\n", t->cood[0][0], t->cood[0][1], t->cood[1][0], t->cood[1][1]);

	b2->dimensions=2;
	b2->cood[0][0]=0;
	b2->cood[0][1]=1;
	b2->cood[1][0]=1;
	b2->cood[1][1]=2;
	t=b2;
	//printf("%d %d %d %d\n", t->cood[0][0], t->cood[0][1], t->cood[1][0], t->cood[1][1]);

	b3->dimensions=2;
	b3->cood[0][0]=0;
	b3->cood[0][1]=1;
	b3->cood[1][0]=2;
	b3->cood[1][1]=3;
	t=b3;
	//printf("%d %d %d %d\n", t->cood[0][0], t->cood[0][1], t->cood[1][0], t->cood[1][1]);

	ht_insert(b1, ht);
	ht_insert(b2, ht);
	ht_insert(b3, ht);

	t = ht_search(b1->dimensions, b1->cood, ht);
	printf("%d %d %d %d\n", t->cood[0][0], t->cood[0][1], t->cood[1][0], t->cood[1][1]);

	t = ht_search(b2->dimensions, b2->cood, ht);
	printf("%d %d %d %d\n", t->cood[0][0], t->cood[0][1], t->cood[1][0], t->cood[1][1]);

	t = ht_search(b3->dimensions, b3->cood, ht);
	printf("%d %d %d %d\n", t->cood[0][0], t->cood[0][1], t->cood[1][0], t->cood[1][1]);

	ht_delete(b1->dimensions, b1->cood, ht);
	printf("b1 should be deleted from HT: %d\n", ht_search(b1->dimensions, b1->cood, ht));

	printf("%d   %d   %d\n", 6/3, 7/3, 8/3);

}

