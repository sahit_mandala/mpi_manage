/*
 * Wenzhao Zhang, wzhang27@ncsu.edu
 *
 * server components(name-node, data-node) start point
 */

#include "amr_staging.h"
#include "util.h"
#include "server_tp.h"
#include <getopt.h>

#define DEFAULT_MAX_TP_SIZE 16 //default max size of threadpool

//default length
int app_set_len=5;  //will also be used in server_nn.c and server_dn.c
int timesteps_len=100;   //will be used in server_nn.c and server_dn.c
static int role_name_len=2;
static int role_data_len=32;
static int role_data_leader_len=8;
static int role_client_len=32;

static int tp_size=0;
static int role_id;
static int server_run=1;

void  nn_process_request(void * arg);
void  dn_process_request(dn_token * d_token, void * msg, int source);

static void usage(void){
	printf("Usage: ./server OPTIONS:\n"
			"--role, -r    Server roles[either %d(NameNode) or %d(DataNode)]\n"
			"--tpsize, -s  ThreadPool size(Optional)\n", ROLE_NAME, ROLE_DATA);
}

static int parse_args(int argc, char *argv[]){
	const char opt_short[] = "r:s:";
	const struct option opt_long[] = {
		{"role",      1,      NULL,   'r'},
		{"tpsize",    1,      NULL,   's'},
		{NULL,        0,      NULL,   0}
	};

	int opt;

	while ((opt = getopt_long(argc, argv, opt_short, opt_long, NULL)) != -1) {
		switch (opt) {
		case 'r':
			role_id = (optarg) ? atoi(optarg) : -1;
			break;
		case 's':
			tp_size = (optarg) ? atoi(optarg) : -1;
			break;
		default:
			output2log("Unknown argument \n");
			return 1;
		}
	}

	if( role_id!=ROLE_DATA && role_id!=ROLE_NAME ){
		output2log("Unknown ROLE_ID\n");
		return -1;
	}

	if(tp_size<=0)
		tp_size=DEFAULT_MAX_TP_SIZE;

	return 0;
}

int main(int argc, char * argv[]){
	void * t_pointer=NULL;
	char hostname[MPI_MAX_PROCESSOR_NAME];
	int total_comm_procs, total_grp_procs;
	int w_rank/*rank in MPI_COMM_WORLD*/, node_rank/*rank on a node*/, staging_rank/*rank in the staging area*/;
	int i, j;
	int live_flag; //0 denotes to quit
	int * all_procs=NULL, * live_procs=NULL, live_count=0;
	int * all_roles=NULL; //hold the roles of all all procs, ordered by rank in the staging_comm
	int * staging_name=NULL, staging_name_count=0; //for data node, holds global ranks of name-node processes
	int * staging_data=NULL, staging_data_count=0; //for name node, holds global ranks of data-node processes
	//int * staging_data_leader=NULL;
	int staging_data_leader_count=0;  //for name node, holds global ranks of leader proc on data node
	MPI_Comm node_comm, nw_comm/*new world comm*/, client_comm;
	MPI_Group w_group, nw_group, client_group;
	char * all_procs_hostnames=NULL;
	int my_hostname_len=0;
	int t_node_leader=0;
	int message_size=0;
	void * message=NULL;
	MPI_Status status;
	int *client_procs, client_procs_count;
	resourcepool ** rscpools=NULL; //for name-node threads; first dimension is how many threads, second dimension is a resourcepool-pointer for each thread
	int assignment_count=0;   //used to assign tasks to each thread in round-robin manner for name-node
	thr_token * t_token=NULL;   //for name node threads
	dn_token * d_token=NULL;   //for data node procs
	//int dn_node_leader=0;  //mark if a proc is node-leader on a data-node
	int provided;
#if AMR_ENABLE_STATISTICS
	double t_start, t_end;
#endif

	if( argc<3 || parse_args(argc, argv) != 0) {
		usage();
		exit(1);
	}
/*#if AMR_SERVER_DEBUG
	output2log("RoleID: %d, TP_Size: %d\n", role_id, tp_size);
#endif  */


	/*Init:*/
    MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, &provided);
    if( role_id==ROLE_NAME && provided!=MPI_THREAD_MULTIPLE ){
    	output2log("WARNING: current thread level(%d) is not MPI_THREAD_MULTIPLE, you might want to reconfigure the environment to improve performance.\n", provided);
    }

	memset(hostname, 0, MPI_MAX_PROCESSOR_NAME);
	MPI_Get_processor_name(hostname, &i);
	MPI_Comm_size(MPI_COMM_WORLD, &total_comm_procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &w_rank);  //get world comm rank
	MPI_Comm_group(MPI_COMM_WORLD, &w_group); //get group info
	MPI_Group_size(w_group, &total_grp_procs);
#if AMR_SERVER_DEBUG
	if(w_rank==MPI_ROOT)
		output2log("server.c; initially(root on host: %s); totally %d procs\n", hostname, total_grp_procs);
#endif


	/*Find out redundant procs for name-node, there should be only one MPI process, mark processes that are unnecessary:*/
	live_flag=PROC_LIVE_FLAG;
	my_hostname_len = strlen(hostname);
	all_procs_hostnames = (char*)allocate_mem(sizeof(char)*MPI_MAX_PROCESSOR_NAME*total_comm_procs+1 , "all_proc_hostnames");
	MPI_Allgather(hostname, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, all_procs_hostnames, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, MPI_COMM_WORLD);
	t_node_leader = total_comm_procs;
	for( i=0; i<total_comm_procs; i++){
		if( strncmp(hostname, all_procs_hostnames+i*MPI_MAX_PROCESSOR_NAME, my_hostname_len)==0 && (strlen(hostname)==strlen(all_procs_hostnames+i*MPI_MAX_PROCESSOR_NAME) ) ){
			if(i<t_node_leader)  //let the one with smallest rank is the node-leader
				t_node_leader = i;
		}
	}
	if(role_id==ROLE_NAME && w_rank!=t_node_leader) //if not first proc on the name-node
		live_flag=PROC_DIE_FLAG;
	if(role_id==ROLE_DATA && w_rank==t_node_leader){  //find a data-node leader
		//dn_node_leader=1;
		role_id = ROLE_DATA_LEADER;
	}
	free(all_procs_hostnames);


	/*Form new global communicator; Unnecessary procs quit:*/
	//to gather all global procs' live-die flag
	all_procs = (int*)allocate_mem(sizeof(int)*total_grp_procs+1, "all_procs");
	MPI_Allgather(&live_flag, 1, MPI_INT, all_procs, 1, MPI_INT, MPI_COMM_WORLD);
	for(i=0, live_count=0; i<total_grp_procs; i++ ){  //count how many live procs
		if(all_procs[i] != PROC_DIE_FLAG) //if live
			live_count++;
	}
	live_procs = (int*)allocate_mem(sizeof(int)*live_count+1, "live_procs");
	for(i=0, j=0; i< total_grp_procs; i++)  //record all live procs's ranks
		if(all_procs[i] != PROC_DIE_FLAG) //if live
			live_procs[j++]=i;
	MPI_Group_incl(w_group, live_count, live_procs, &nw_group);
	MPI_Comm_create(MPI_COMM_WORLD, nw_group, &nw_comm);  //if redundant procs exit before this create-operation, it will stuck there
	if( !live_flag ){  //redundant processes exit
/*
#if AMR_SERVER_DEBUG
		output2log("server.c; w_rank: %d, on host: %s, I'm unnecessary, to exit\n", w_rank, hostname);
#endif
*/
		MPI_Finalize();
		exit(0);
	}
	free(live_procs);
	MPI_Group_free(&w_group);


	/*Name-nodes get all data-nodes' ranks;  Data-nodes get all name-nodes' ranks:*/
	//get new total_proc and new world_rank
	MPI_Comm_size(nw_comm, &total_comm_procs);
	MPI_Comm_rank(nw_comm, &w_rank);
#if AMR_SERVER_DEBUG
	//output2log("server.c; w_rank: %d, on host: %s, role_id: %d\n", w_rank, hostname, role_id);
	if(w_rank==MPI_ROOT)
		output2log("server.c; after make sure only one proc on each server node(root on host: %s); totally %d procs\n", hostname, total_comm_procs);
#endif
	all_roles = (int*)allocate_mem(sizeof(int)*total_comm_procs+1, "all_roles");
	MPI_Allgather(&role_id, 1, MPI_INT, all_roles, 1, MPI_INT, nw_comm);
/* #if AMR_SERVER_DEBUG
	if(w_rank==MPI_ROOT){
		output2log("all_roles: ");
		for(i=0; i< total_comm_procs; i++)
			output2log("%d, ", all_roles[i]);
		output2log("\n");
	}
#endif */
	if(role_id == ROLE_NAME){ //name-node needs to get the new global ranks of all data-node
		staging_data = (int*)allocate_mem(sizeof(int)*role_data_len+1, "staging_data");
		staging_data_count=0;
		//staging_data_leader  = (int*)allocate_mem(sizeof(int)*role_data_leader_len+1, "staging_data");
		staging_data_leader_count=0;
		for(i=0; i<total_comm_procs; i++){
			if( all_roles[i]==ROLE_DATA || all_roles[i]==ROLE_DATA_LEADER ){
				staging_data[staging_data_count++] = i;
			}

			if( all_roles[i]==ROLE_DATA_LEADER ){
				//staging_data_leader[staging_data_leader_count++] = i;
				staging_data_leader_count++;
			}

			if(staging_data_count==role_data_len){ //if staging_data buffer insufficient
				t_pointer = (int*)allocate_mem(sizeof(int)*role_data_len*2+1, "t_pointer");  //allocate double size
				memcpy(t_pointer, staging_data, sizeof(int)*role_data_len);
				free(staging_data);
				role_data_len*=2;  //double max possible size
				staging_data = t_pointer;
				t_pointer = NULL;
			}

			/*if( staging_data_leader_count == role_data_leader_len){
				t_pointer = (int*)allocate_mem(sizeof(int)*role_data_leader_len*2+1, "t_pointer");  //allocate double size
				memcpy(t_pointer, staging_data_leader, sizeof(int)*role_data_leader_len);
				free(staging_data_leader);
				role_data_leader_len*=2;
				staging_data_leader = t_pointer;
				t_pointer = NULL;
			}  */
		}   //end for

		if( staging_data_count!=0 && staging_data_leader_count==0){ //this condition might happen on Sith, when configured improperly, different roles of procs could run on the same node, causing problems
			staging_data_leader_count=1;
			//staging_data_leader[0] = staging_data[0];
		}

#if AMR_SERVER_DEBUG
		output2log("server.c; NameNode(w_rank:%d, on host: %s); totally %d data-node procs; %d leaders of data-nodes; ",
				w_rank, hostname, staging_data_count, staging_data_leader_count);
/*		output2log("server.c; NameNode(w_rank:%d, on host: %s), all known(%d) data-nodes w_ranks: ", w_rank, hostname, staging_data_count);
		for(i=0; i<staging_data_count; i++)
			output2log("%d, ", staging_data[i]);
		output2log("\n");  */
#endif
	}

	//all data-node&name-node need to get the new global ranks of all name-nodes and client-nodes
	staging_name = (int*)allocate_mem(sizeof(int)*role_name_len+1, "staging_name");
	staging_name_count = 0;
	client_procs = (int*) allocate_mem(sizeof(int)*role_client_len+1, "client_procs");  //no real use on server side, just cooperation with clients
	client_procs_count = 0;
	for(i=0; i<total_comm_procs; i++){
		if( all_roles[i]==ROLE_NAME ){
			staging_name[staging_name_count++] = i;
		}

		if( all_roles[i]<ROLE_NAME || all_roles[i]>ROLE_DATA_LEADER ){ //find a client
			client_procs[client_procs_count++] = i;
		}

		if(staging_name_count==role_name_len){
			t_pointer = (int*)allocate_mem(sizeof(int)*role_name_len*2+1, "t_pointer");
			memcpy(t_pointer, staging_name, sizeof(int)*role_name_len);
			free(staging_name);
			role_name_len*=2;
			staging_name = t_pointer;
			t_pointer=NULL;
		}

		if(client_procs_count == role_client_len){
			t_pointer = (int*)allocate_mem(sizeof(int)*role_client_len*2+1, "t_pointer");
			memcpy(t_pointer, client_procs, sizeof(int)*role_client_len);
			free(client_procs);
			role_client_len*=2;
			client_procs = t_pointer;
			t_pointer=NULL;
		}
	}
	//creating client comm to cooperate with clients, or client-side corresponding creation will hang there....
	MPI_Group_incl(nw_group, client_procs_count, client_procs, &client_group);
	MPI_Comm_create(nw_comm, client_group, &client_comm);
	free(all_roles);
	free(client_procs);
	MPI_Group_free(&client_group);
	//MPI_Comm_free(&client_comm);   //<-- cause unknown error, program fail

/*#if AMR_SERVER_DEBUG
	if(role_id == ROLE_DATA){
		output2log("server.c; DataNode(w_rank:%d, on host: %s), all known(%d) name-nodes w_ranks: ", w_rank, hostname, staging_name_count);
		for(i=0; i<staging_name_count; i++)
			output2log("%d, ", staging_name[i]);
		output2log("\n");

	}  //end if-else to get count&ranks of name-nodes and data-nodes
#endif  */


	//to init tokens for name-node and data-node
	switch(role_id){
	case ROLE_NAME:   //init thread token for namenode; every threads on a node will share the same copy
		t_token = (thr_token*)allocate_mem(sizeof(thr_token)+1, "");
		t_token->staging_name = staging_name;
		t_token->staging_name_count = staging_name_count;
		t_token->staging_data = staging_data;
		t_token->staging_data_count = staging_data_count;
		//t_token->staging_data_leader = staging_data_leader;
		t_token->staging_data_leader_count = staging_data_leader_count;
		t_token->staging_data_proc_node = staging_data_count / staging_data_leader_count;  //how many procs on a datanode; here suppose every datanode has the same number of procs
		t_token->nw_comm = nw_comm;
		t_token->nw_group = nw_group;
		t_token->nw_rank = w_rank;
		t_token->app_count=0;
		t_token->app_set = (client_app**)allocate_mem(sizeof(client_app*)*app_set_len+1, "");
		t_token->staging_data_proc_workload = (unsigned long*)allocate_mem(sizeof(unsigned long)*staging_data_count+1, "");  //data-node proc workload lock
		t_token->staging_data_proc_workload_locks = (pthread_rwlock_t*)allocate_mem(sizeof(pthread_rwlock_t)*staging_data_count+1, "");
		t_token->staging_data_node_workload = (unsigned long*)allocate_mem(sizeof(unsigned long)*staging_data_leader_count+1, "");   //data-node workload lock
		t_token->staging_data_node_workload_locks = (pthread_rwlock_t*)allocate_mem(sizeof(pthread_rwlock_t)*staging_data_leader_count+1, "");
		pthread_rwlock_init(&t_token->app_set_lock, NULL);
		for(i=0; i<staging_data_count; i++){
			pthread_rwlock_init(&(t_token->staging_data_proc_workload_locks[i]), NULL);
		}
		for(i=0; i<staging_data_leader_count; i++){
			pthread_rwlock_init(&(t_token->staging_data_node_workload_locks[i]), NULL);
		}
		t_token->tp_size = tp_size;

		/*setup resource-pool for every thread for name-node*/
		rscpools = (resourcepool**)allocate_mem( sizeof(resourcepool*)*tp_size + 1, "");
		if(role_id==ROLE_NAME){
			for(i=0; i<tp_size; i++)
				rscpools[i] = create_resourcepool(t_token, nn_process_request, i);
		}

#if AMR_ENABLE_STATISTICS   //every thread on the name-node has its own statistics info
		t_token->statistics = (double**)allocate_mem(sizeof(double*)*tp_size+1, "");
		for(i=0; i<tp_size; i++)
			t_token->statistics[i] = (double*)allocate_mem(sizeof(double)*M_GET_STATISTICS_NN_FIX_DOUBLE+1, "");  //to store max/min
		t_token->stat_server_recv = 0.0;
#endif

		break;

	case ROLE_DATA_LEADER:
	case ROLE_DATA:  //init dn_token for each proc on datanodes.
		d_token = (dn_token*)allocate_mem(sizeof(dn_token)+1, "");
		d_token->staging_name = staging_name;
		d_token->staging_name_count = staging_name_count;
		d_token->nw_comm = nw_comm;
		d_token->nw_group = nw_group;
		d_token->nw_rank = w_rank;
		d_token->app_count=0;
		d_token->app_set = (client_app**)allocate_mem(sizeof(client_app*)*app_set_len+1, "");
		//d_token->node_leader = dn_node_leader;

#if AMR_ENABLE_STATISTICS
		d_token->statistics = (double*)allocate_mem(sizeof(double)*M_GET_STATISTICS_DN_FIX_DOUBLE+1, "");
#endif
		break;
	}

	/* listen for incoming msg;
	 * for name-node, only one MPI process on each node, the proc will add new received msg as new tasks to each thread's resource pool;
	 * for data-node, multiple MPI procs on each node(no threading for each procs), when a proc receives msg, just go ahead handle it.
	 */
	while(server_run){
		MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, nw_comm, &status);  //int MPI_Probe(int source, int tag, MPI_Comm comm, MPI_Status *status)
		MPI_Get_count(&status, MPI_CHAR, &message_size);
		message = allocate_mem(message_size+1, "server.c; message");

#if AMR_ENABLE_STATISTICS
		t_start = dclock();
#endif

		MPI_Recv(message, message_size, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, nw_comm, &status);  //recv actual msg data

#if AMR_ENABLE_STATISTICS    //the actual value might don't make sense(too large), because procs start waiting at the beginning
		t_end = dclock();
		switch(role_id){
		case ROLE_NAME:
			t_token->stat_server_recv += (t_end - t_start);
			break;
		case ROLE_DATA_LEADER:
		case ROLE_DATA:
			d_token->statistics[STAT_SERVER_RECV_INDEX] += (t_end - t_start);
			break;
		}
#endif

		switch(role_id){
		case ROLE_NAME:
			add_tp_work_elem( rscpools[assignment_count++], message, status.MPI_SOURCE);  /*message is the raw received msg*/
			if(assignment_count==tp_size)
				assignment_count=0;
			break;
		case ROLE_DATA_LEADER:
		case ROLE_DATA:
			dn_process_request(d_token, message, status.MPI_SOURCE);
			break;
		}  //end of switch-case
	}

	MPI_Finalize();
	exit(0);
}



