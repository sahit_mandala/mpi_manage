/*
 * Wenzhao Zhang, wzhang27@ncsu.edu
 * Server NameNode events processing functions, "nn_process_request" is used to start threads
 */

#include "server_tp.h"
#include <float.h>
#include <math.h>
#include <limits.h>


//default len
extern int app_set_len;
extern int timesteps_len;

static int poolid;  //private to the thread

//index for statistics, private to the thread
#if AMR_ENABLE_STATISTICS
static const int t_put_box_workload=0;
static const int t_put_box_search_ts=1;
static const int t_put_box_search_create_ts=2;
static const int t_put_box_search_level=3;
static const int t_put_box_search_create_level=4;
static const int t_put_box_insert_ht=5;

static const int t_get_box_search=6;
static const int t_get_box_search_ht=7;

static const int t_put_total=8;
static const int t_get_total=9;

static const int t_process_req=10;
#endif


static void process_reg_app_id(thr_token * t_token, thr_work_elem * work_elem){
	client_app * t_app = NULL;
	void * t_pointer=NULL;
	int data[4];
	int i, j;  //, j=0, temp;
	//MPI_Request * reqs;

	//extract info
	t_app = (client_app*)allocate_mem(sizeof(client_app)+1, "");
	t_app->dimensions = ((int*)work_elem->msg)[1];
	t_app->max_levels = ((int*)work_elem->msg)[2];
	t_app->ts_level_box = (box_hash_table**)allocate_mem(sizeof(box_hash_table*)*timesteps_len+1, "");
	t_app->timesteps_len = timesteps_len;  //app specific variable
	t_app->ts_locks = (pthread_rwlock_t*)allocate_mem(sizeof(pthread_rwlock_t)*timesteps_len+1, "");
	t_app->level_locks = (pthread_rwlock_t**)allocate_mem(sizeof(pthread_rwlock_t*)*timesteps_len+1, "");
	for(i=0; i<timesteps_len; i++){
		pthread_rwlock_init(&(t_app->ts_locks[i]), NULL);   //init timestep locks

		//init level_locks for each timestep
		t_app->level_locks[i] = (pthread_rwlock_t*)allocate_mem(sizeof(pthread_rwlock_t)*t_app->max_levels+1, "");
		for(j=0; j<t_app->max_levels; j++)
			pthread_rwlock_init(&(t_app->level_locks[i][j]), NULL);
	}
	pthread_rwlock_init(&t_app->ts_level_lock, NULL);

	//update client_token's app structure
	pthread_rwlock_wrlock(& t_token->app_set_lock);
	t_token->app_set[t_token->app_count] = t_app;
	t_app->id = ++t_token->app_count;  //in-lock assignment guarantees continuous increasing app ids
	if(t_token->app_count==app_set_len){  //expand set if full
		t_pointer = allocate_mem(sizeof(client_app)*app_set_len*2+1, "");
		memcpy(t_pointer, t_token->app_set, sizeof(client_app)*app_set_len);
		free(t_token->app_set);
		app_set_len*=2;
		t_token->app_set = t_pointer;
		t_pointer=NULL;
	}
	pthread_rwlock_unlock(& t_token->app_set_lock);
/*#if AMR_SERVER_DEBUG
	output2log("Namenode(%d) finished new app registration: %d\n", t_token->nw_rank, t_app->id);
#endif  */

	//notify other namenodes & datanodes
	data[0]=M_NOTIFY_APP_ID;
	data[1]=t_app->id;
	data[2]=t_app->dimensions;
	data[3]=t_app->max_levels;
	for(i=0; i<t_token->staging_data_count; i++){  //notify the new app info with all data-nodes
		MPI_Send(data, 4, MPI_INT, t_token->staging_data[i], M_NOTIFY_APP_ID, t_token->nw_comm);  //, MPI_REQUEST_NULL);  //, &reqs[j++]);
	}
	for(i=1;i <t_token->staging_name_count; i++){  //notify the new app info with all other potential name-nodes
		MPI_Send(data, 4, MPI_INT, t_token->staging_name[i], M_NOTIFY_APP_ID, t_token->nw_comm);  //, MPI_REQUEST_NULL);  //, &reqs[j++]);
	}

	//finally send back msg to client API, this will make sure the new-app info is distributed to all staging nodes before client API could use it to push info
	MPI_Send(&t_app->id, 1, MPI_INT, work_elem->source, M_REG_APP_ID, t_token->nw_comm);   //, MPI_REQUEST_NULL);
	free(work_elem->msg);
	free(work_elem);
}

static void process_notify_app_id(thr_token * t_token, thr_work_elem * work_elem){
	client_app * t_app = NULL;
	void * t_pointer=NULL;
	int i, j;

	t_app = (client_app*)allocate_mem(sizeof(client_app)+1, "");
	t_app->id = ((int*)work_elem->msg)[1];
	t_app->dimensions = ((int*)work_elem->msg)[2];
	t_app->max_levels = ((int*)work_elem->msg)[3];
	t_app->ts_level_box = (box_hash_table**)allocate_mem(sizeof(box_hash_table*)*timesteps_len+1, "");
	t_app->timesteps_len = timesteps_len;
	t_app->ts_locks = (pthread_rwlock_t*)allocate_mem(sizeof(pthread_rwlock_t)*timesteps_len+1, "");
	t_app->level_locks = (pthread_rwlock_t**)allocate_mem(sizeof(pthread_rwlock_t*)*timesteps_len+1, "");
	for(i=0; i<timesteps_len; i++){
		pthread_rwlock_init(&(t_app->ts_locks[i]), NULL);  //init timestep locks

		//init level_locks for each timestep
		t_app->level_locks[i] = (pthread_rwlock_t*)allocate_mem(sizeof(pthread_rwlock_t)*t_app->max_levels+1, "");
		for(j=0; j<t_app->max_levels; j++)
			pthread_rwlock_init(&(t_app->level_locks[i][j]), NULL);
	}
	pthread_rwlock_init(&t_app->ts_level_lock, NULL);

	pthread_rwlock_wrlock(& t_token->app_set_lock);
	t_token->app_set[t_token->app_count] = t_app;
	++t_token->app_count;

	if(t_token->app_count==app_set_len){  //expand set if full
		t_pointer = allocate_mem(sizeof(client_app)*app_set_len*2+1, "");
		memcpy(t_pointer, t_token->app_set, sizeof(client_app)*app_set_len);
		free(t_token->app_set);
		app_set_len*=2;
		t_token->app_set = t_pointer;
		t_pointer=NULL;
	}
	pthread_rwlock_unlock(& t_token->app_set_lock);
/*#if AMR_SERVER_DEBUG
	output2log("Namenode(%d) gets notified new app: %d\n", t_token->nw_rank, t_app->id);
#endif*/

	free(work_elem->msg);
	free(work_elem);
}

static void process_reg_push_box(thr_token * t_token, thr_work_elem * work_elem){
	AMR_INT * reg_data=NULL;
	AMR_INT box_size;
	AMR_INT box_key;  //used as original hash-key
	int app_id, timestep, level, ts_index;
	int i, j, t, dn_proc_index, dn_node_index;
	int dn_rank;
	unsigned long load, t_load;
	client_app * ca=NULL;
	void * t_pointer;
	box * b=NULL;
	//MPI_Request req;

#if AMR_ENABLE_STATISTICS
	double t_workload_start, t_workload_end;
	double t_search_ts_start, t_search_ts_end;
	double t_search_create_ts_start, t_search_create_ts_end;
	double t_search_level_start, t_search_level_end;
	double t_search_create_level_start, t_search_create_level_end;
	double t_insert_ht_start, t_insert_ht_end;
	double t_total_start, t_total_end;
#endif

#if AMR_ENABLE_STATISTICS
	t_total_start = dclock();
#endif

	if(t_token->staging_data_count==0){  //no data-node
		dn_rank=-1;
		MPI_Send(&dn_rank, 1, MPI_INT, work_elem->source, M_REG_PUSH_BOX, t_token->nw_comm);   //, &req); //send back msg,    MPI_REQUEST_NULL
		return;
	}

	//find data-node with smallest load
#if AMR_ENABLE_STATISTICS
	t_workload_start = dclock();
#endif
	//first find the data-node with smallest load
	if( t_token->staging_data_leader_count!=1 ){
		dn_node_index=-1;
		load = ULONG_MAX;
		for(i=0; i<t_token->staging_data_leader_count; i++){
			if( pthread_rwlock_tryrdlock( &(t_token->staging_data_node_workload_locks[i]) )==0 ){ //lock successful
				t_load = t_token->staging_data_node_workload[i];
				pthread_rwlock_unlock( &(t_token->staging_data_node_workload_locks[i]) );

				if( t_load<load ){
					load = t_load;
					dn_node_index=i;
				}
			}
		}
		if(dn_node_index==-1){
			dn_node_index = rand() % t_token->staging_data_leader_count;
		}
	}else{   //if only one data-node; no need to care about the single datanode's workload
		dn_node_index=0;
	}

	//then find the procs on the found data-node with smallest workload
	dn_proc_index=-1;
	load = ULONG_MAX;
	t = (1+dn_node_index)*t_token->staging_data_proc_node;
	for( i=dn_node_index*t_token->staging_data_proc_node;  i<t;  i++){
		if( pthread_rwlock_tryrdlock( &(t_token->staging_data_proc_workload_locks[i]) )==0 ){ //lock successful
			t_load = t_token->staging_data_proc_workload[i];
			pthread_rwlock_unlock( &(t_token->staging_data_proc_workload_locks[i]) );

			if( t_load<load ){
				load = t_load;
				dn_proc_index=i;
			}
		}
	}
	if(dn_proc_index==-1){  //randomly choose one
		//srand( time(NULL) );
		dn_proc_index = rand() % t_token->staging_data_count;
	}
#if AMR_ENABLE_STATISTICS
	t_workload_end = dclock();
	t_token->statistics[poolid][t_put_box_workload] += (t_workload_end - t_workload_start);
#endif

	//extract info
	dn_rank = t_token->staging_data[dn_proc_index];
	reg_data = (AMR_INT*)work_elem->msg;
	app_id = reg_data[1];
	timestep = reg_data[2];
	level = reg_data[3];
	box_key = reg_data[4];
	box_size = reg_data[5];
/*	if( app_id>t_token->app_count ){
		free(work_elem->msg);
		free(work_elem);
#if AMR_SERVER_DEBUG
		output2log("ERROR: server_nn.c, process_reg_push_box, invalid app_id: %d > %d\n", app_id, t_token->app_count);
#endif
		return;
	}   */
	pthread_rwlock_rdlock(& t_token->app_set_lock);
	ca = t_token->app_set[app_id-1];
	pthread_rwlock_unlock(& t_token->app_set_lock);
	if(ca==NULL || ca->id!=app_id){
		free(work_elem->msg);
		free(work_elem);
#if AMR_SERVER_DEBUG
		output2log("ERROR: server_nn.c, process_reg_push_box, get NULL app, or inconsistent app_id: %d != %d\n", ca!=NULL ? ca->id : -1, app_id);
#endif
		return;
	}
	b = allocate_box(ca->dimensions);
	b->dn_rank = dn_rank;
/*#if AMR_SERVER_DEBUG
	output2log("server_nn.c, get PUSH-box request, app_id: %d, timestep: %d, level: %d, box_key: %d, box_size: %d, current TS len: %d\n",
													app_id, timestep, level, box_key, box_size, ca->timesteps_len);
#endif   */
	switch(ca->dimensions){
	case AMR_2D:
		b->cood[0][0] = reg_data[M_REG_PUSH_BOX_FIX_PART];
		b->cood[0][1] = reg_data[M_REG_PUSH_BOX_FIX_PART+1];
		b->cood[1][0] = reg_data[M_REG_PUSH_BOX_FIX_PART+2];
		b->cood[1][1] = reg_data[M_REG_PUSH_BOX_FIX_PART+3];
		break;
	case AMR_3D:
		b->cood[0][0] = reg_data[M_REG_PUSH_BOX_FIX_PART];
		b->cood[0][1] = reg_data[M_REG_PUSH_BOX_FIX_PART+1];
		b->cood[1][0] = reg_data[M_REG_PUSH_BOX_FIX_PART+2];
		b->cood[1][1] = reg_data[M_REG_PUSH_BOX_FIX_PART+3];
		b->cood[2][0] = reg_data[M_REG_PUSH_BOX_FIX_PART+4];
		b->cood[2][1] = reg_data[M_REG_PUSH_BOX_FIX_PART+5];
		break;
	default:
		return ;
	}

	//update load table
#if AMR_ENABLE_STATISTICS
	t_workload_start = dclock();
#endif
	if( t_token->staging_data_leader_count!=1 ){
		pthread_rwlock_wrlock( &(t_token->staging_data_node_workload_locks[dn_node_index]) );
		t_token->staging_data_node_workload[dn_node_index] += box_size;
		pthread_rwlock_unlock( &(t_token->staging_data_node_workload_locks[dn_node_index]) );
	}

	pthread_rwlock_wrlock( &(t_token->staging_data_proc_workload_locks[dn_proc_index]) );
	t_token->staging_data_proc_workload[dn_proc_index] += box_size;
	pthread_rwlock_unlock( &(t_token->staging_data_proc_workload_locks[dn_proc_index]) );
#if AMR_ENABLE_STATISTICS
	t_workload_end = dclock();
	t_token->statistics[poolid][t_put_box_workload] += (t_workload_end - t_workload_start);
#endif

	//update the app's box hashtable
	ts_index = (int)(timestep/t_token->staging_name_count);  //use the global TS to map the index of the TS_hashtable in current name-node
	if( ts_index>=ca->timesteps_len ){ //need to expend timestep's array structures
		pthread_rwlock_wrlock(&ca->ts_level_lock);
		if( ts_index>=ca->timesteps_len ){
#if AMR_SERVER_DEBUG
			output2log("server_nn.c, reg_put_box, going to expand TS hashtables, current size: %d\n", ca->timesteps_len);
#endif
			t = ca->timesteps_len*2;  //k is new timestep length

			//expend TS hashtable-array:
			t_pointer = allocate_mem(sizeof(box_hash_table*)*t+1, "");  //expand
			memcpy(t_pointer, ca->ts_level_box, sizeof(box_hash_table*)*ca->timesteps_len);  //copy
			free(ca->ts_level_box);
			ca->ts_level_box=t_pointer;

			//expand locks array for each timestep: "ts_locks"
			t_pointer = allocate_mem(sizeof(pthread_rwlock_t)*t+1, "");  //expand
			memcpy(t_pointer, ca->ts_locks, sizeof(pthread_rwlock_t)*ca->timesteps_len);  //copy
			free(ca->ts_locks);
			ca->ts_locks = t_pointer;

			//expand locks array for each timestep's each level: "level_locks"
			t_pointer = allocate_mem(sizeof(pthread_rwlock_t*)*t+1, "");   //expand
			memcpy(t_pointer, ca->level_locks, sizeof(pthread_rwlock_t*)*ca->timesteps_len);  //copy
			free(ca->level_locks);
			ca->level_locks = t_pointer;

			for(i=ca->timesteps_len; i<t; i++){
				pthread_rwlock_init(& (ca->ts_locks[i]), NULL);  //init new timestep locks

				//allocate & init new level locks
				ca->level_locks[i] = (pthread_rwlock_t*)allocate_mem(sizeof(pthread_rwlock_t)*ca->max_levels+1, "");
				for(j=0; j<ca->max_levels; j++)
					pthread_rwlock_init(&(ca->level_locks[i][j]), NULL);
			}

			//finally enlarge this value
			ca->timesteps_len=t;
			t_pointer=NULL;
		}
		pthread_rwlock_unlock(&ca->ts_level_lock);
	}  //end if needed to expand TS structures


#if AMR_ENABLE_STATISTICS
	t_search_ts_start = dclock();
#endif
	//to protect the case: needing to expend the TS array
	pthread_rwlock_rdlock(&ca->ts_level_lock);
	MPI_Send(&dn_rank, 1, MPI_INT, work_elem->source, M_REG_PUSH_BOX, t_token->nw_comm); //, &req); //send reply to client, indicate to which data-node to store the data;
	if( ca->ts_level_box[ts_index]==NULL ){  //the timestep's structure is uninitialized
		pthread_rwlock_wrlock( &(ca->ts_locks[ts_index]) );
		//it is possible that more than one threads found the structure is NULL at the same time, so at required the lock, check it again.
		if( ca->ts_level_box[ts_index]==NULL ){
#if AMR_ENABLE_STATISTICS
			t_search_create_ts_start = dclock();
#endif
			ca->ts_level_box[ts_index] = (box_hash_table *)allocate_mem(sizeof(box_hash_table)*ca->max_levels+1, "");
#if AMR_ENABLE_STATISTICS
			t_search_create_ts_end = dclock();
			t_token->statistics[poolid][t_put_box_search_create_ts] += (t_search_create_ts_end - t_search_create_ts_start);
#endif
		}
		pthread_rwlock_unlock( &(ca->ts_locks[ts_index]) );
	}
#if AMR_ENABLE_STATISTICS
	t_search_ts_end = dclock();
	t_token->statistics[poolid][t_put_box_search_ts] += (t_search_ts_end-t_search_ts_start);
#endif


#if AMR_ENABLE_STATISTICS
	t_search_level_start = dclock();
#endif
	if( ca->ts_level_box[ts_index][level]==NULL ){
		pthread_rwlock_wrlock( &(ca->level_locks[ts_index][level]) );
		if( ca->ts_level_box[ts_index][level]==NULL ){
#if AMR_ENABLE_STATISTICS
			t_search_create_level_start = dclock();
#endif
			ca->ts_level_box[ts_index][level] = ht_create_concurrent(HT_ARRAY_SIZE_M, t_token->tp_size);
#if AMR_ENABLE_STATISTICS
			t_search_create_level_end = dclock();
			t_token->statistics[poolid][t_put_box_search_create_level] += (t_search_create_level_end - t_search_create_level_start);
#endif
		}
		pthread_rwlock_unlock( &(ca->level_locks[ts_index][level]) );
	}
	pthread_rwlock_unlock(&ca->ts_level_lock);
#if AMR_ENABLE_STATISTICS
	t_search_level_end = dclock();
	t_token->statistics[poolid][t_put_box_search_level] += (t_search_level_end - t_search_level_start);
#endif


#if AMR_ENABLE_STATISTICS
	t_insert_ht_start = dclock();
#endif
	ht_insert_key_concurrent(b, ca->ts_level_box[ts_index][level], box_key, b->cood[1][1]);
#if AMR_ENABLE_STATISTICS
	t_insert_ht_end = dclock();
	t_token->statistics[poolid][t_put_box_insert_ht] += (t_insert_ht_end - t_insert_ht_start);
#endif

	free(work_elem->msg);
	free(work_elem);

#if AMR_ENABLE_STATISTICS
	t_total_end = dclock();
	t_token->statistics[poolid][t_put_total] += (t_total_end - t_total_start);
#endif
}

static void process_get_box(thr_token * t_token, thr_work_elem * work_elem){
	int req_data_len=0;
	AMR_INT * req_data=NULL/*received msg from client*/,  * req_data_dn/*req msg to be sent to data-node*/;
	AMR_INT box_size;
	AMR_INT box_key;  //used as original hash-key
	int app_id, timestep, level, ts_index;
	int i, dn_rank;
	client_app * ca=NULL;
	AMR_INT ** cood=NULL;
	box_hash_table * timestep_hts=NULL; //one TS's hashtables
	box_hash_table level_ht=NULL; //one level's hashtable
	box * b=NULL;
	//MPI_Request req;
	AMR_DATA_TYPE search_error=0;
	int error_flag=0;

#if AMR_ENABLE_STATISTICS
	double t_search_start, t_search_end;
	double t_search_ht_start, t_search_ht_end;
	double t_total_start, t_total_end;
#endif

#if AMR_ENABLE_STATISTICS
	t_total_start = dclock();
#endif

	//extract info
	req_data = (AMR_INT*)work_elem->msg;
	app_id = req_data[1];
	timestep = req_data[2];
	level = req_data[3];
	box_key = req_data[4];
	box_size = req_data[5];
	pthread_rwlock_rdlock(& t_token->app_set_lock);
	ca = t_token->app_set[app_id-1];
	pthread_rwlock_unlock(& t_token->app_set_lock);
	if(ca==NULL || ca->id!=app_id){
		free(work_elem->msg);
		free(work_elem);
#if AMR_SERVER_DEBUG
		output2log("ERROR: server_nn.c, process_get_box, get NULL app, or inconsistent app_id: %d != %d\n", ca!=NULL ? ca->id : -1, app_id);
#endif
		return;
	}
	cood = allocate_coodinates(ca->dimensions);
/*#if AMR_SERVER_DEBUG
	output2log("server_nn.c, get GET-box request, app_id: %d, timestep: %d, level: %d, box_key: %d, box_size: %d\n", app_id, timestep, level, box_key, box_size);
#endif*/
	switch(ca->dimensions){
	case AMR_2D:
		cood[0][0] = req_data[M_GET_BOX_FIX_PART];
		cood[0][1] = req_data[M_GET_BOX_FIX_PART+1];
		cood[1][0] = req_data[M_GET_BOX_FIX_PART+2];
		cood[1][1] = req_data[M_GET_BOX_FIX_PART+3];
		break;
	case AMR_3D:
		cood[0][0] = req_data[M_GET_BOX_FIX_PART];
		cood[0][1] = req_data[M_GET_BOX_FIX_PART+1];
		cood[1][0] = req_data[M_GET_BOX_FIX_PART+2];
		cood[1][1] = req_data[M_GET_BOX_FIX_PART+3];
		cood[2][0] = req_data[M_GET_BOX_FIX_PART+4];
		cood[2][1] = req_data[M_GET_BOX_FIX_PART+5];
		break;
	default:
		return ;
	}


	//search the hashtables for the box
#if AMR_ENABLE_STATISTICS
	t_search_start = dclock();
#endif
	ts_index = (int)(timestep/t_token->staging_name_count);
	if( ts_index<ca->timesteps_len){  //ensure timestep valid
		pthread_rwlock_rdlock(& (ca->ts_locks[ts_index]) );
		timestep_hts = ca->ts_level_box[ts_index];
		pthread_rwlock_unlock(& (ca->ts_locks[ts_index]) );
		if(timestep_hts!=NULL){   //ensure timestep's hashtable
			pthread_rwlock_rdlock(& (ca->level_locks[ts_index][level]) );
			level_ht = timestep_hts[level];
			pthread_rwlock_unlock(& (ca->level_locks[ts_index][level]) );

			if(level_ht != NULL){  //ensure level's hashtable valid
#if AMR_ENABLE_STATISTICS
				t_search_ht_start = dclock();
#endif
				b = ht_search_key_concurrent(ca->dimensions, cood, level_ht, box_key, cood[1][1]);
#if AMR_ENABLE_STATISTICS
				t_search_ht_end = dclock();
				t_token->statistics[poolid][t_get_box_search_ht] += (t_search_ht_end-t_search_ht_start);
#endif

				if(b==NULL){
					error_flag=1;
#if AMR_SERVER_DEBUG
					output2log("ERROR: server_nn.c, process_get_box, box is NULL for ts_index: %d and level: %d\n", ts_index, level);
#endif
				}
			}else{
				error_flag=1;
#if AMR_SERVER_DEBUG
				output2log("ERROR: server_nn.c, process_get_box, level_ht is NULL for ts_index: %d and level: %d\n", ts_index, level);
#endif
			}

		}else{
			error_flag=1;
#if AMR_SERVER_DEBUG
			output2log("ERROR: server_nn.c, process_get_box, timestep_hts is NULL for ts_index: %d\n", ts_index);
#endif
		}
	}else{
		error_flag=1;
#if AMR_SERVER_DEBUG
		output2log("ERROR: server_nn.c, process_get_box, invalid timestep: %d >= %d\n", ts_index, ca->timesteps_len);
#endif
	}
#if AMR_ENABLE_STATISTICS
	t_search_end = dclock();
	t_token->statistics[poolid][t_get_box_search] += (t_search_end-t_search_start);
#endif

	//send data
	if( !error_flag ){  //find correct datanode, forward get request to the datanode
		req_data_len = M_GET_BOX_FIX_PART+ca->dimensions*2;
		req_data_dn = (AMR_INT*)allocate_mem( sizeof(AMR_INT)*(req_data_len+1), "" );  //need to add the client's rank
		memcpy(req_data_dn, req_data, sizeof(AMR_INT)*req_data_len);
		req_data_dn[req_data_len] = work_elem->source;  //record client's rank, send it to data-node
		MPI_Send(req_data_dn, req_data_len+1, MPI_INT, b->dn_rank, M_GET_BOX, t_token->nw_comm);   //, &req);
	}else{
		search_error=-1.0;
		MPI_Send(&search_error, 1, AMR_MPI_DATA_TYPE, work_elem->source, M_GET_BOX, t_token->nw_comm);   //, &req);
	}

	free(work_elem->msg);
	free(work_elem);
	//MPI_Wait(&req, MPI_STATUSES_IGNORE);
	if( !error_flag )
		free(req_data_dn);

#if AMR_ENABLE_STATISTICS
	t_total_end = dclock();
	t_token->statistics[poolid][t_get_total] += (t_total_end - t_total_start);
#endif

	return;
}

#if AMR_ENABLE_STATISTICS
static void process_get_statistics(thr_token * t_token, thr_work_elem * work_elem){
	double * vars=NULL;
	int i, j, k;
	double max, min;

	//the first int is nn_rank, the other doubles are time records
	vars = (double*)allocate_mem( (2*M_GET_STATISTICS_NN_FIX_DOUBLE+1)*sizeof(double) + 1, "");

	//find biggest and smallest
	k=0;
	for(i=0; i<M_GET_STATISTICS_NN_FIX_DOUBLE; i++){   //iterate over each variable
		max = 0.0;
		min = 9000000.0;
		for(j=0; j< t_token->tp_size; j++){    //for each variable, find the max/min value among all threads

			if( t_token->statistics[j][i] > max )
				max = t_token->statistics[j][i];

			if( t_token->statistics[j][i] < min )
				min = t_token->statistics[j][i];
		}

		vars[k++] = max;
		vars[k++] = min;
	}
	vars[k] = t_token->stat_server_recv;

/*
	vars[0] = t_put_box_workload;
	vars[1] = t_put_box_search_ts;
	vars[2] = t_put_box_search_create_ts;
	vars[3] = t_put_box_search_level;
	vars[4] = t_put_box_search_create_level;
	vars[5] = t_put_box_insert_ht;

	vars[6] = t_get_box_search;
	vars[7] = t_get_box_search_ht;

	vars[8] = t_put_total;
	vars[9] = t_get_total;

	vars[10] = t_process_req;
*/

	MPI_Send(vars, 2*M_GET_STATISTICS_NN_FIX_DOUBLE+1, MPI_DOUBLE, work_elem->source, M_GET_STATISTICS, t_token->nw_comm);

	free(work_elem->msg);
	free(work_elem);
	free(vars);
}
#endif

/* this is the thread executing function, it gets a client's request-msg from threadpool's work-list, reads
 * message types and assigns concrete task to corresponding functions. */
void  nn_process_request(void * arg){
	resourcepool * tp;
	thr_token * t_token;
	thr_work_elem * work_elem;
	int msg_type;

#if AMR_ENABLE_STATISTICS
	double t_start, t_end;
#endif

	if(arg==NULL){
		return;
	}

	tp = (resourcepool*)arg;
	t_token = tp->t_token;
	poolid = tp->poolid;

	while(tp->tp_run){

#if AMR_ENABLE_STATISTICS
		t_start = dclock();
#endif

		pthread_mutex_lock( &tp->tp_request_mutex );
		while(tp->req_head==NULL){ //while no request element
			pthread_cond_wait( &tp->tp_request_cond, &tp->tp_request_mutex );
		}
		work_elem = get_tp_work_elem(tp);  //this elem is the received raw data
		pthread_mutex_unlock( &tp->tp_request_mutex );

#if AMR_ENABLE_STATISTICS
		t_end = dclock();
		//the actual value might don't make sense(too large), because some threads might not get a task for a very long time....
		t_token->statistics[poolid][t_process_req] += (t_end - t_start);
#endif

		if(work_elem==NULL){
#if AMR_SERVER_DEBUG
			output2log("ERROR: server_nn.c, nn_process_request, NULL work_elem\n");
#endif
			continue;
		}

		//cast it to int*, get the first msg-type int
		msg_type = ((int*)work_elem->msg)[0];

		//switch-case on msg-types, those routine functions are responsible for finally free the raw message:
		switch(msg_type){
		case M_REG_APP_ID:   //client
			process_reg_app_id(t_token, work_elem);
			break;
		case M_NOTIFY_APP_ID:
			process_notify_app_id(t_token, work_elem);
			break;
		case M_REG_PUSH_BOX:
			process_reg_push_box(t_token, work_elem);
			break;
		case M_GET_BOX:
			process_get_box(t_token, work_elem);
			break;
#if AMR_ENABLE_STATISTICS
		case M_GET_STATISTICS:
			process_get_statistics(t_token, work_elem);
			break;
#endif
		default:
			output2log("ERROR: server_nn.c; unknown msg_types: %d\n", msg_type);
			free(work_elem->msg);
			free(work_elem);
		}

	}  //end while

}
