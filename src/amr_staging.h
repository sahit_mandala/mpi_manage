/*
 * Wenzhao Zhang, wzhang27@ncsu.edu
 */

#ifndef AMR_STAGING_H_
#define AMR_STAGING_H_

#include <stdlib.h>
#include <string.h>
#include <mpi.h>

//debug flags
#define AMR_SERVER_DEBUG 1
#define AMR_CLIENT_DEBUG 1
#define AMR_ENABLE_STATISTICS 0

//all roles
#define ROLE_NAME 1  //name node
#define ROLE_DATA 2  //data node
#define ROLE_DATA_LEADER 3  //a data-node's leader
#define ROLE_CLIENT 4

#define MPI_ROOT 0

#define PROC_DIE_FLAG 0
#define PROC_LIVE_FLAG 1
//#define PROC_LIVE_DN_LEADER 2 //data-node leader process

//#define RESOURCE_POOL_REQ_SIZE 2

//AMR levels
#define AMR_2D 2
#define AMR_3D 3


//msg types: the first int is always msg type.
/*
 * Client root register APP ID with the first name-node. Msg format:
 * 		first int: msg type,
 * 		second int: dimensions,
 * 		third int:  max levels
 * 	Name node reply: just an app id
 */
#define M_REG_APP_ID 1

/*
 * The first name-node notify other possible name-nodes the new app id
 *
 */
#define M_NOTIFY_APP_ID 2

/*
 * Client API wants to register a box-push operation with the server, msg sent to name-node
 * 		1) int: msg_type
 * 		2) int: app_id
 * 		3) int: timestep
 * 		4) int: AMR level
 * 		5) int: box key
 * 		6) int: box size
 * 		7) a set of AMR_INT, represents box's coordinates
 * 	Name node reply: just a data-node rank to which the actual box will be sent; -1 denotes error, possibly no datanode
 */
#define M_REG_PUSH_BOX 3
#define M_REG_PUSH_BOX_FIX_PART 6

/*
 * Client API push the actual box's info to server(data-node)
 * 		1) int: msg type
 * 		2) int: app_id
 * 		3) int: timestep
 * 		4) int: AMR level
 * 		5) int: box key
 * 		6) int: box size
 * 		7) a set of AMR_INT, represents box's coordinates
 * 		8) actual data
 */
#define M_PUSH_BOX 4
#define M_PUSH_BOX_DATA 44  //put actual binary data
#define M_PUSH_BOX_FIX_PART 6  //count
//#define M_PUSH_BOX_MEMCPY_LIMIT 4194304  //2048*2048 double values, 32 MB
//#define M_PUSH_BOX_MEMCPY_LIMIT 1048576    //1024*1024 double values, 8 MB
/*
 * Client API want to get a box, could be sent to either name-node or data-node
 * 		1) int: msg_type
 * 		2) int: app_id
 * 		3) int: timestep
 * 		4) int: AMR level      //not needed by data-node
 * 		5) int: box key
 * 		6) int: box size
 * 		7) a set of AMR_INT, represents box's coordinates
 */
#define M_GET_BOX 5
#define M_GET_BOX_FIX_PART 6  //count

#define M_GET_STATISTICS 6
#define M_GET_STATISTICS_NN_FIX_DOUBLE 11
#define M_GET_STATISTICS_DN_FIX_DOUBLE 12


typedef int AMR_INT;
typedef unsigned int AMR_UINT;
typedef long AMR_LINT;
typedef unsigned long AMR_ULINT;
typedef double AMR_DATA_TYPE;
#define  AMR_MPI_DATA_TYPE  MPI_DOUBLE
//#define  AMR_MPI_INT MPI_INT

typedef struct _box_st{
	AMR_INT dimensions;
	//cood: the first dimension is a list of coordinates for a certain axis(for instance x-axis), the second dimension contains low/high coordinates for the axis(from X->Z)
	AMR_INT ** cood;
	AMR_DATA_TYPE * data;   //<--- default is double

	int dn_rank;
}box;

#endif
