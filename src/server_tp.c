/*
 * Wenzhao Zhang, wzhang27@ncsu.edu
 *
 * server pthread-based thread-pool
 */

#include "server_tp.h"

//default len

resourcepool * create_resourcepool(thr_token* t_token, const dispatch_fn to_exe, int poolid){
	resourcepool * pool;

	pool = (resourcepool *) allocate_mem(sizeof(resourcepool)+1, "resourcepool");
	pool->req_head = NULL;
	pool->req_end = NULL;
	pool->t_token = t_token;
	pool->tp_run = 1;
	pool->poolid = poolid;
	pthread_mutex_init(&pool->tp_request_mutex, NULL);
	pthread_cond_init(&pool->tp_request_cond, NULL);

	if ( pthread_create( &pool->tid, NULL, to_exe, pool )!=0 ) {
		output2log("ERROR: server_tp.c; create_resourcepool, pthread_create error\n");
		exit(1);
	}

	return pool;
}

//add to list tail
int add_tp_work_elem( resourcepool * rscpool, void * msg, int source){
	thr_work_elem * e=NULL;

	if(rscpool == NULL || msg==NULL)
		return 1;

	e = (thr_work_elem*)allocate_mem(sizeof(thr_work_elem)+1, "");
	e->msg = msg;
	e->source = source;
	e->next = NULL;

	pthread_mutex_lock(&rscpool->tp_request_mutex);
	if(rscpool->req_head==NULL){
		rscpool->req_head = e;
		rscpool->req_end = e;
	}
	else{
		rscpool->req_end->next = e;
		rscpool->req_end = e;
	}
	pthread_mutex_unlock(&rscpool->tp_request_mutex);

	pthread_cond_signal( &rscpool->tp_request_cond );

	return 0;
}

/*
 * Get from head
 * Used with conditional variables, caller of this function should lock the req-mutex, this function will not internally lock the mutex
 */
thr_work_elem * get_tp_work_elem(resourcepool * tp){
	thr_work_elem * elem;

	if(tp == NULL)
		return NULL;

	//pthread_mutex_lock(&pool->tp_request_mutex);
	if(tp->req_head == NULL)
		return NULL;
	else{
		elem = tp->req_head;   /* get req from the list head */
		tp->req_head = tp->req_head->next;

		if(tp->req_head == NULL) /* if only has one elem in list */
			tp->req_end = NULL;

	}
	//pthread_mutex_unlock(&pool->tp_request_mutex);

	return elem;
}

void destroy_threadpool(resourcepool * destroyme){
	resourcepool *pool = destroyme;
	thr_work_elem * e;
	if(pool==NULL)
		return;

	pthread_mutex_lock (&pool->tp_request_mutex);
	pool->tp_run=0;  //no longer accept request
	while(pool->req_head!=NULL){
		e = pool ->req_head;
		pool ->req_head = pool ->req_head->next;
		free(e);
	}
	pool ->req_head = pool ->req_end =NULL;
	pthread_mutex_unlock(&pool->tp_request_mutex);

	pthread_mutex_destroy(&pool->tp_request_mutex);
	pthread_cond_destroy(&pool->tp_request_cond);

	free(destroyme);

	return;
}
