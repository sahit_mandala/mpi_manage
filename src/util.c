/*
 * Wenzhao Zhang, wzhang27@ncsu.edu
 */

//#include <stdlib.h>
#include "util.h"

//only specific to util.c:
#include <unistd.h>  //only needed for variable args
#include <stdarg.h>
//#include <stddef.h>

/* ----------start define hash-table structs---------- */
typedef struct _ht_list_st{
	int elem_number; /* number of elements in the list */
	_ht_list_elem * l_head;
	_ht_list_elem * l_tail;
}_ht_list;

typedef struct _hash_table_st {
	_ht_list ** array;
	int size; /* array's length, not actual number of elements in the hash table.  tiny, small, medium, large */
} _hash_table;
/* ----------end define hash-table structs---------- */


void * allocate_mem(unsigned int size, char * msg){
	void * data=NULL;

	if(size<=0)
		return NULL;

	data = malloc(size);
	if(data==NULL){
		printf("ERROR: allocate_mem, out of memory! -- %s\n", msg);
		exit(1);
	}
	memset(data, 0, size);
	return data;
}

/*hash functions*/
uint64_t fasthash64(const void *buf, size_t len, uint64_t seed){
	const uint64_t    m = 0x880355f21e6d1965ULL;
	const uint64_t *pos = (const uint64_t *)buf;
	const uint64_t *end = pos + (len / 8);
	const unsigned char *pos2;
	uint64_t h = seed ^ (len * m);
	uint64_t v;

	while (pos != end) {
		v  = *pos++;
		h ^= mix(v);
		h *= m;
	}

	pos2 = (const unsigned char*)pos;
	v = 0;

	switch (len & 7) {
	case 7: v ^= (uint64_t)pos2[6] << 48;
	case 6: v ^= (uint64_t)pos2[5] << 40;
	case 5: v ^= (uint64_t)pos2[4] << 32;
	case 4: v ^= (uint64_t)pos2[3] << 24;
	case 3: v ^= (uint64_t)pos2[2] << 16;
	case 2: v ^= (uint64_t)pos2[1] << 8;
	case 1: v ^= (uint64_t)pos2[0];
		h ^= mix(v);
		h *= m;
	}

	return mix(h);
}
uint32_t fasthash32(const void *buf, size_t len, uint32_t seed){
    uint64_t h = fasthash64(buf, len, seed);  //use consistent seed to guarantee consistent hash-values
	return abs( h - (h >> 32) );
}


/* ----------start define Concurrent hash-table functions---------- */
/* reference: http://blog.csdn.net/gdujian0119/article/details/6777239
 * http://www.cse.yorku.ca/~oz/hash.html */
static _ht_list * create_htlist(){
	_ht_list * l_t = NULL;

	l_t = (_ht_list*)allocate_mem(sizeof(_ht_list)+1, "");

	l_t->elem_number = 0;
	l_t->l_head = NULL;
	l_t->l_tail = NULL;

	return l_t;
}

/*
 * key: hash key
 * data: the actual data of the box
 */
static _ht_list_elem * create_htlist_elem(int key, void * data){
	_ht_list_elem * le_t = NULL;

	if(data==NULL){
#if AMR_UTIL_DEBUG
		printf("create_htlist_elem, key or data is NULL\n");
#endif
		return NULL;
	}

	le_t = (_ht_list_elem*)allocate_mem( sizeof(_ht_list_elem)+1, "" );

	le_t->key = key;
	le_t->data = data;
	le_t->next = NULL;

	return le_t;
}

static int en_htlist(_ht_list_elem * e_t, _ht_list * l_t){  //insert element to list
	if(e_t==NULL || l_t==NULL){
#if AMR_UTIL_DEBUG
		printf("en_htlist, list_elem or list is NULL.\n");
#endif
		return 1;
	}

    if(l_t->elem_number==0){ /* if list empty */
    	l_t->l_head = l_t->l_tail = e_t;
    }else{
    	l_t->l_tail->next = e_t;
    	l_t->l_tail = e_t;
    }
    l_t->elem_number++;

    return 0;
}

/*
 * only remove the element-struct from the list, not free the actual data
 */
static int del_htlist_elem(AMR_INT dimensions, AMR_INT ** cood, _ht_list * l){
	_ht_list_elem * lp1=NULL, * lp2=NULL, * to_del=NULL;
	int i, size, found_flag=0;
	box * b=NULL;

	if(dimensions<=1 || cood==NULL || l==NULL){
#if AMR_UTIL_DEBUG
		printf("del_htlist_elem, invalid args\n");
#endif
		return 1;
	}

    size = l->elem_number;
    lp1 = l->l_head;
    for(i=0; i< size; i++){
    	b = (box*)lp1->data;
		switch(dimensions){
		case AMR_2D:
			if( b->cood[0][0]==cood[0][0] && b->cood[0][1]==cood[0][1] && b->cood[1][0]==cood[1][0] && b->cood[1][1]==cood[1][1] )
				found_flag=1;
			break;
		case AMR_3D:
			if( b->cood[0][0]==cood[0][0] && b->cood[0][1]==cood[0][1] && b->cood[1][0]==cood[1][0] && b->cood[1][1]==cood[1][1] && b->cood[2][0]==cood[2][0] && b->cood[2][1]==cood[2][1])
				found_flag=1;
			break;
		}

    	if( found_flag ){ /* if found */
    		to_del=lp1;
    		if(lp1==l->l_head){ /* if to_del is the head */
    			if(l->elem_number==1){ /* if only one elem in the list */
    				l->l_head = l->l_tail = NULL;
    			}else{
    				l->l_head = l->l_head->next;
    			}
    			break;
    		} else if(lp1==l->l_tail){
    			l->l_tail = lp2;
    			lp2->next = NULL;
    			break;
    		}else{
    			lp2->next = lp1->next;
    			break;
    		}
    	}

    	lp2=lp1;
    	lp1 = lp1->next;
    } /* end for */

    if(to_del!=NULL){
    	to_del->next = NULL; /* cut relation with the list */
    	l->elem_number--;
    }

    return to_del!=NULL ? 0 : 1;
}

/*Concurrent HashTable public interfaces:*/
box_hash_table ht_create(int size){
	_hash_table * ht = NULL;
	int i;

	if(size<=0){
#if AMR_UTIL_DEBUG
		printf("ht_create, size<=0.\n");
#endif
		return NULL;
	}

	ht = (_hash_table*)allocate_mem(sizeof(_hash_table)+1, "");

	ht->array = (_ht_list**)allocate_mem( sizeof(_ht_list*)*size + 1, "" );
	ht->size = size;

	return (box_hash_table)ht;
}

/*
 * key: the sum of a box's coordinates
 */
int ht_insert_key(box * b, box_hash_table t, AMR_INT key, uint32_t seed){
	int ht_index=0;
	_hash_table * ht = NULL;

	if(b==NULL || t==NULL){
#if AMR_UTIL_DEBUG
		printf("ht_insert, b(%d) or t(%d) is NULL.\n", b, t);
#endif
		return 1;
	}

	ht = (_hash_table*)t;
	ht_index = fasthash32(&key, HASH_KEY_LEN, seed) % ht->size;

	if(ht->array[ht_index]==NULL){
		if(ht->array[ht_index]==NULL)
			ht->array[ht_index] = create_htlist();
	}

	en_htlist( create_htlist_elem(key, b), ht->array[ht_index] );

	return 0;
}
/*
 * b: the actual pointer of the box struct
 */
int ht_insert(box * b, box_hash_table t, uint32_t seed){
	int ht_index=0, partition;
	AMR_INT key;
	_hash_table * ht = NULL;

	if(b==NULL || t==NULL){
#if AMR_UTIL_DEBUG
		printf("ht_insert, b(%d) or t(%d) is NULL.\n", b, t);
#endif
		return 1;
	}

	switch(b->dimensions){
	case AMR_2D:
		key = b->cood[0][0] + b->cood[0][1] + b->cood[1][0] + b->cood[1][1];
		break;
	case AMR_3D:
		key = b->cood[0][0] + b->cood[0][1] + b->cood[1][0] + b->cood[1][1] + b->cood[2][0] + b->cood[2][1];
		break;
	default:
		return 1;
	}
	ht = (_hash_table*)t;
	ht_index = fasthash32(&key, HASH_KEY_LEN, seed) % ht->size;  //_SEED

	if(ht->array[ht_index]==NULL){
		if(ht->array[ht_index]==NULL)
			ht->array[ht_index] = create_htlist();
	}

	en_htlist( create_htlist_elem(key, b), ht->array[ht_index] );

	return 0;
}

box * ht_search_key(AMR_INT dimensions, AMR_INT ** cood, box_hash_table t, AMR_INT key, uint32_t seed){
	int ht_index=0;
	_hash_table * ht = NULL;
	_ht_list_elem * ht_le = NULL;
	box * b=NULL;

	if(dimensions<=1 || cood==NULL || t==NULL){
#if AMR_UTIL_DEBUG
		printf("ht_search, invalid args, dimensions(%d), cood(%d), t(%d)\n", dimensions, cood, t);
#endif
		return NULL;
	}

	ht = (_hash_table*)t;
	ht_index = fasthash32(&key, HASH_KEY_LEN, seed) % ht->size;

	if(ht->array[ht_index]==NULL){
		return NULL;
	}

	ht_le = ht->array[ht_index]->l_head;
	while(ht_le!=NULL){
		if( key == ht_le->key ){
			b = (box*)ht_le->data;
			switch(dimensions){
			case AMR_2D:
				if( b->cood[0][0]==cood[0][0] && b->cood[0][1]==cood[0][1] && b->cood[1][0]==cood[1][0] && b->cood[1][1]==cood[1][1] )
					return b;
				break;
			case AMR_3D:
				if( b->cood[0][0]==cood[0][0] && b->cood[0][1]==cood[0][1] && b->cood[1][0]==cood[1][0] && b->cood[1][1]==cood[1][1] && b->cood[2][0]==cood[2][0] && b->cood[2][1]==cood[2][1])
					return b;
				break;
			}
		}
		ht_le = ht_le->next;
	}

	return NULL;

}
box * ht_search(AMR_INT dimensions, AMR_INT ** cood, box_hash_table t, uint32_t seed){
	int ht_index=0;
	_hash_table * ht = NULL;
	_ht_list_elem * ht_le = NULL;
	AMR_INT key;
	box * b=NULL;

	if(dimensions<=1 || cood==NULL || t==NULL){
#if AMR_UTIL_DEBUG
		printf("ht_search, invalid args, dimensions(%d), cood(%d), t(%d)\n", dimensions, cood, t);
#endif
		return NULL;
	}

	switch(dimensions){
	case AMR_2D:
		key = cood[0][0] + cood[0][1] + cood[1][0] + cood[1][1];
		break;
	case AMR_3D:
		key = cood[0][0] + cood[0][1] + cood[1][0] + cood[1][1] + cood[2][0] + cood[2][1];
		break;
	default:
		return NULL;
	}

	ht = (_hash_table*)t;
	ht_index = fasthash32(&key, HASH_KEY_LEN, seed) % ht->size;

	if(ht->array[ht_index]==NULL){
		return NULL;
	}

	ht_le = ht->array[ht_index]->l_head;
	while(ht_le!=NULL){
		if( key == ht_le->key ){
			b = (box*)ht_le->data;
			switch(dimensions){
			case AMR_2D:
				if( b->cood[0][0]==cood[0][0] && b->cood[0][1]==cood[0][1] && b->cood[1][0]==cood[1][0] && b->cood[1][1]==cood[1][1] )
					return b;
				break;
			case AMR_3D:
				if( b->cood[0][0]==cood[0][0] && b->cood[0][1]==cood[0][1] && b->cood[1][0]==cood[1][0] && b->cood[1][1]==cood[1][1] && b->cood[2][0]==cood[2][0] && b->cood[2][1]==cood[2][1])
					return b;
				break;
			}
		}
		ht_le = ht_le->next;
	}

	return NULL;
}

int ht_delete(AMR_INT dimensions, AMR_INT ** cood, box_hash_table t){
	_ht_list_elem * le = NULL;
	int ht_index=0;
	_hash_table * ht = NULL;
	_ht_list_elem * ht_le = NULL;
	AMR_INT key;

	if(dimensions<=1 || cood==NULL || t==NULL){
#if AMR_UTIL_DEBUG
		printf("ht_search, invalid args.\n");
#endif
		return NULL;
	}

	switch(dimensions){
	case AMR_2D:
		key = cood[0][0] + cood[0][1] + cood[1][0] + cood[1][1];
		break;
	case AMR_3D:
		key = cood[0][0] + cood[0][1] + cood[1][0] + cood[1][1] + cood[2][0] + cood[2][1];
		break;
	default:
		return NULL;
	}

	ht = (_hash_table*)t;
	ht_index = fasthash32(&key, HASH_KEY_LEN, cood[1][1]) % ht->size;

	if(ht->array[ht_index]==NULL)
		return 1;

	return del_htlist_elem(dimensions, cood, ht->array[ht_index]);
}

void ht_destruct(box_hash_table h){
	_hash_table * ht = NULL;
	_ht_list_elem * le1 = NULL, * le2=NULL;
	int i=0, s=0;
	if(h==NULL)
		return;

	ht = (_hash_table*)h;
	s = ht->size;
	for(i=0;i<s;i++){
		if(ht->array[i]==NULL)
			continue;

		le1 = ht->array[i]->l_head;
		while (le1 != NULL) { /* iterate through an element of the ht, */
			le2 = le1;
			le1 = le1->next;

			le2->data = NULL;
			le2->key = NULL;
			le2->next = NULL;
			free(le2);  /* free list element(_symbol) */
		}
		free(ht->array[i]); /* free list itself */
		ht->array[i]=NULL;
	}

	free(ht);
}
/* ----------end define concurrent hash-table functions---------- */



void output2log(const char * format, ...) {
	va_list msg_list;

	va_start(msg_list, format);

	vprintf(format, msg_list);

	va_end(msg_list);
}

AMR_INT ** allocate_coodinates(AMR_INT dimensions){
	AMR_INT ** cood=NULL;
	int  i=0;

	if(dimensions<=1){
#if AMR_UTIL_DEBUG
		printf("allocate_coordinates, dimensions invalid: %d\n", dimensions);
#endif
		return NULL;
	}

	cood = (AMR_INT**)malloc(sizeof(AMR_INT*)*dimensions+1);
	memset(cood, 0, sizeof(AMR_INT*)*dimensions+1);

	for(i=0; i<dimensions; i++){
		cood[i] = (AMR_INT*)malloc(sizeof(AMR_INT)*2+1);
		memset(cood[i], 0, sizeof(AMR_INT)*2+1);
	}

	return cood;
}

box * allocate_box(AMR_INT dimensions){
	int  i=0;
	box * b=NULL;

	if(dimensions<=1){
#if AMR_UTIL_DEBUG
		printf("allocate_box, dimensions invalid: %d\n", dimensions);
#endif
		return NULL;
	}

	b = (box*)malloc(sizeof(box)+1);
	if(b==NULL)
		return NULL;
	memset(b, 0, sizeof(box)+1);
	b->dimensions=dimensions;

	b->cood = (AMR_INT**)malloc(sizeof(AMR_INT*)*dimensions+1);
	memset(b->cood, 0, sizeof(AMR_INT*)*dimensions+1);

	for(i=0; i<dimensions; i++){
		b->cood[i] = (AMR_INT*)malloc(sizeof(AMR_INT)*2+1);
		memset(b->cood[i], 0, sizeof(AMR_INT)*2+1);
	}
	b->dn_rank=-1;
	b->data=NULL;
	return b;
}

void destruct_box(box * b){
	int i;
	if(b==NULL)
		return;

	for(i=0; i<b->dimensions; i++)
		free(b->cood[i]);
	free(b->cood);
	free(b);

	if(b->data!=NULL)
		free(b->data);

	b=NULL;
}

int SDBMHash(char *str) {
	int hash = 0;

	while (*str) {
		hash = (*str++) + (hash << 6) + (hash << 16) - hash;
	}

	return (hash & 0x7FFFFFFF);
}

double dclock(){
	struct timeval tv;
	gettimeofday(&tv,0);
	return (double) tv.tv_sec + (double) tv.tv_usec * 1e-6;
}
