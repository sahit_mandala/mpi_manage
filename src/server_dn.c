/*
 * Wenzhao Zhang, wzhang27@ncsu.edu
 * Server DataNode event processing functions, "dn_process_request" is used to start threads
 */

#include "server_tp.h"

//default len
extern int app_set_len;
extern int timesteps_len;

//index
#if AMR_ENABLE_STATISTICS
static const int t_put_box_search_ts=0;
static const int t_put_box_search_create_ts=1;
static const int t_put_box_search_level=2;
static const int t_put_box_search_create_level=3;
static const int t_put_box_insert_ht=4;

static const int t_get_box_search=5;
static const int t_get_box_search_ht=6;
static const int t_get_box_send=7;

static const int t_put_total=8;
static const int t_get_total=9;
static const int s_workload=10;
#endif

static void process_notify_app_id(dn_token * d_token, void * msg){
	client_app * t_app = NULL;
	void * t_pointer=NULL;

	t_app = (client_app*)allocate_mem(sizeof(client_app)+1, "");
	t_app->id = ((int*)msg)[1];
	t_app->dimensions = ((int*)msg)[2];
	t_app->max_levels = ((int*)msg)[3];
	t_app->ts_level_box = (box_hash_table**)allocate_mem(sizeof(box_hash_table*)*timesteps_len+1, "");
	t_app->timesteps_len = timesteps_len;

	d_token->app_set[d_token->app_count] = t_app;
	++d_token->app_count;

	if(d_token->app_count==app_set_len){  //expand set if full
		t_pointer = allocate_mem(sizeof(client_app)*app_set_len*2+1, "");
		memcpy(t_pointer, d_token->app_set, sizeof(client_app)*app_set_len);
		free(d_token->app_set);
		app_set_len*=2;
		d_token->app_set = t_pointer;
		t_pointer=NULL;
	}
/*#if AMR_SERVER_DEBUG
	output2log("Datanode(%d) gets notified new app: %d\n", t_token->nw_rank, t_app->id);
#endif   */

	free(msg);
}

//new function to adapt to client's new push_box
static void process_push_box(dn_token * d_token, void * msg, int source){
	void * push_data=NULL;
	AMR_INT * push_data_int=NULL;
	AMR_INT box_size;
	AMR_INT box_key;  //used as original hash-key
	int app_id, timestep, level;
	int i, j, t, xwidth, ywidth;
	client_app * ca=NULL;
	void * t_pointer;
	box * b=NULL;
	MPI_Request reqs;

#if AMR_ENABLE_STATISTICS
	double t_search_ts_start, t_search_ts_end;
	double t_search_create_ts_start, t_search_create_ts_end;
	double t_search_level_start, t_search_level_end;
	double t_search_create_level_start, t_search_create_level_end;
	double t_insert_ht_end, t_insert_ht_start;
	double t_total_start, t_total_end;
#endif

#if AMR_ENABLE_STATISTICS
	t_total_start = dclock();
#endif

	//extract info
	push_data=msg;
	push_data_int = (AMR_INT*)msg;
	app_id = push_data_int[1];
	timestep = push_data_int[2];
	level = push_data_int[3];
	box_key = push_data_int[4];
	box_size = push_data_int[5];

	ca = d_token->app_set[app_id-1];
	if(ca==NULL || ca->id!=app_id){
		free(msg);
#if AMR_SERVER_DEBUG
		output2log("ERROR: server_dn.c, process_push_box, get NULL app, or inconsistent app_id: %d != %d\n", ca!=NULL ? ca->id : -1, app_id);
#endif
		return;
	}
	b = allocate_box(ca->dimensions);
	b->data = (AMR_DATA_TYPE*)allocate_mem(sizeof(AMR_DATA_TYPE)*box_size+1, "");  //to receive actual data
	MPI_Irecv(b->data, box_size, AMR_MPI_DATA_TYPE, source, M_PUSH_BOX_DATA, d_token->nw_comm, &reqs);

/*#if AMR_SERVER_DEBUG
	output2log("server_dn.c, get PUSH-box request, app_id: %d, timestep: %d, level: %d, box_key: %d, box_size: %d, current TS len: %d\n",
															app_id, timestep, level, box_key, box_size, ca->timesteps_len);
#endif   */
	switch(ca->dimensions){
	case AMR_2D:
		b->cood[0][0] = push_data_int[M_PUSH_BOX_FIX_PART];
		b->cood[0][1] = push_data_int[M_PUSH_BOX_FIX_PART+1];
		b->cood[1][0] = push_data_int[M_PUSH_BOX_FIX_PART+2];
		b->cood[1][1] = push_data_int[M_PUSH_BOX_FIX_PART+3];
		break;
	case AMR_3D:
		b->cood[0][0] = push_data_int[M_PUSH_BOX_FIX_PART];
		b->cood[0][1] = push_data_int[M_PUSH_BOX_FIX_PART+1];
		b->cood[1][0] = push_data_int[M_PUSH_BOX_FIX_PART+2];
		b->cood[1][1] = push_data_int[M_PUSH_BOX_FIX_PART+3];
		b->cood[2][0] = push_data_int[M_PUSH_BOX_FIX_PART+4];
		b->cood[2][1] = push_data_int[M_PUSH_BOX_FIX_PART+5];
		break;
	default:
		return ;
	}

	//update the app's box hashtable
	if( timestep>=ca->timesteps_len ){ //need to expend timestep's array structures
#if AMR_SERVER_DEBUG
		output2log("server_dn.c, put_box, going to expand TS hashtables, current size: %d\n", ca->timesteps_len);
#endif
		t = ca->timesteps_len*2;  //t is new timestep length

		//expend TS hashtable-array:
		t_pointer = allocate_mem(sizeof(box_hash_table*)*t+1, "");  //expand
		memcpy(t_pointer, ca->ts_level_box, sizeof(box_hash_table*)*ca->timesteps_len);  //copy
		free(ca->ts_level_box);
		ca->ts_level_box=t_pointer;

		//finally enlarge this value
		ca->timesteps_len=t;
		t_pointer=NULL;
	}

#if AMR_ENABLE_STATISTICS
	t_search_ts_start = dclock();
#endif
	if( ca->ts_level_box[timestep]==NULL ){  //the timestep's structure is uninitialized
#if AMR_ENABLE_STATISTICS
			t_search_create_ts_start = dclock();
#endif
			ca->ts_level_box[timestep] = (box_hash_table *)allocate_mem(sizeof(box_hash_table)*ca->max_levels+1, "");
#if AMR_ENABLE_STATISTICS
			t_search_create_ts_end = dclock();
			d_token->statistics[t_put_box_search_create_ts] += (t_search_create_ts_end - t_search_create_ts_start);
#endif
	}
#if AMR_ENABLE_STATISTICS
	t_search_ts_end = dclock();
	d_token->statistics[t_put_box_search_ts] += (t_search_ts_end-t_search_ts_start);
#endif


#if AMR_ENABLE_STATISTICS
	t_search_level_start = dclock();
#endif
	if( ca->ts_level_box[timestep][level]==NULL ){
#if AMR_ENABLE_STATISTICS
		t_search_create_level_start = dclock();
#endif
		ca->ts_level_box[timestep][level] = ht_create(HT_ARRAY_SIZE_M);
#if AMR_ENABLE_STATISTICS
		t_search_create_level_end = dclock();
		d_token->statistics[t_put_box_search_create_level] += (t_search_create_level_end - t_search_create_level_start);
#endif
	}
#if AMR_ENABLE_STATISTICS
	t_search_level_end = dclock();
	d_token->statistics[t_put_box_search_level] += (t_search_level_end - t_search_level_start);
#endif


#if AMR_ENABLE_STATISTICS
	t_insert_ht_start = dclock();
#endif
	ht_insert_key(b, ca->ts_level_box[timestep][level], box_key, b->cood[1][1]);
#if AMR_ENABLE_STATISTICS
	t_insert_ht_end = dclock();
	d_token->statistics[t_put_box_insert_ht] += (t_insert_ht_end - t_insert_ht_start);
#endif


#if AMR_ENABLE_STATISTICS
	t_total_end = dclock();
	d_token->statistics[t_put_total] += (t_total_end - t_total_start);
	d_token->statistics[s_workload] += box_size;
#endif

	MPI_Wait(&reqs, MPI_STATUSES_IGNORE);    //<---- might cause performance bottleneck
}


static void process_get_box(dn_token * d_token, void * msg, int source){
	int req_data_len=0;
	AMR_INT * req_data=NULL/*received msg from client*/;
	AMR_INT box_size;
	AMR_INT box_key;  //used as original hash-key
	int app_id, timestep, level;
	int i, client_rank=-1;
	client_app * ca=NULL;
	AMR_INT ** cood=NULL;
	box_hash_table * timestep_hts=NULL; //one TS's hashtables
	box_hash_table level_ht=NULL; //one level's hashtable
	box * b=NULL;
	//MPI_Request req;
	AMR_DATA_TYPE search_error=0;
	int error_flag=0;

#if AMR_ENABLE_STATISTICS
	double t_search_start, t_search_end;
	double t_search_ht_start, t_search_ht_end;
	double t_total_start, t_total_end;
	double t_send_start, t_send_end;
#endif

#if AMR_ENABLE_STATISTICS
	t_total_start = dclock();
#endif

	//extract info
	req_data = (AMR_INT*)msg;
	app_id = req_data[1];
	timestep = req_data[2];
	level = req_data[3];
	box_key = req_data[4];
	box_size = req_data[5];
	ca = d_token->app_set[app_id-1];
	if(ca==NULL || ca->id!=app_id){
		free(msg);
#if AMR_SERVER_DEBUG
		output2log("ERROR: server_dn.c, process_get_box, get NULL app, or inconsistent app_id: %d != %d\n", ca!=NULL ? ca->id : -1, app_id);
#endif
		return;
	}
	for(i=0; i<d_token->staging_name_count; i++){
		if( source==d_token->staging_name[i]){
			client_rank=req_data[M_GET_BOX_FIX_PART+ca->dimensions*2]; //msg from name-node, set client_rank from the last int of msg
			break;
		}
	}
	if(client_rank==-1){ //flag not modified in the above for-loop, means msg from client, set client_rank
		client_rank =  source;
	}

	cood = allocate_coodinates(ca->dimensions);
/*#if AMR_SERVER_DEBUG
	output2log("server_dn.c, get GET-box request, app_id: %d, timestep: %d, level: %d, box_key: %d, box_size: %d, client_rank: %d\n", app_id, timestep, level, box_key, box_size, client_rank);
#endif*/
	switch(ca->dimensions){
	case AMR_2D:
		cood[0][0] = req_data[M_GET_BOX_FIX_PART];
		cood[0][1] = req_data[M_GET_BOX_FIX_PART+1];
		cood[1][0] = req_data[M_GET_BOX_FIX_PART+2];
		cood[1][1] = req_data[M_GET_BOX_FIX_PART+3];
		break;
	case AMR_3D:
		cood[0][0] = req_data[M_GET_BOX_FIX_PART];
		cood[0][1] = req_data[M_GET_BOX_FIX_PART+1];
		cood[1][0] = req_data[M_GET_BOX_FIX_PART+2];
		cood[1][1] = req_data[M_GET_BOX_FIX_PART+3];
		cood[2][0] = req_data[M_GET_BOX_FIX_PART+4];
		cood[2][1] = req_data[M_GET_BOX_FIX_PART+5];
		break;
	default:
		return ;
	}

	//search the hashtables for the box
#if AMR_ENABLE_STATISTICS
	t_search_start = dclock();
#endif
	if( timestep < ca->timesteps_len){  //ensure timestep valid
		timestep_hts = ca->ts_level_box[timestep];
		if(timestep_hts!=NULL){   //ensure timestep's hashtable
			level_ht = timestep_hts[level];

			if(level_ht != NULL){  //ensure level's hashtable valid
#if AMR_ENABLE_STATISTICS
				t_search_ht_start = dclock();
#endif
				b = ht_search_key(ca->dimensions, cood, level_ht, box_key, cood[1][1]);
#if AMR_ENABLE_STATISTICS
				t_search_ht_end = dclock();
				d_token->statistics[t_get_box_search_ht] += (t_search_ht_end-t_search_ht_start);
#endif

				if(b==NULL){
					error_flag=1;
#if AMR_SERVER_DEBUG
					output2log("ERROR: server_dn.c, process_get_box, box is NULL for ts_index: %d and level: %d\n", timestep, level);
#endif
				}
			}else{
				error_flag=1;
#if AMR_SERVER_DEBUG
				output2log("ERROR: server_dn.c, process_get_box, level_ht is NULL for ts_index: %d and level: %d\n", timestep, level);
#endif
			}

		}else{
			error_flag=1;
#if AMR_SERVER_DEBUG
			output2log("ERROR: server_dn.c, process_get_box, timestep_hts is NULL for ts_index: %d\n", timestep);
#endif
		}
	}else{
		error_flag=1;
#if AMR_SERVER_DEBUG
		output2log("ERROR: server_dn.c, process_get_box, invalid timestep: %d >= %d\n", timestep, ca->timesteps_len);
#endif
	}
#if AMR_ENABLE_STATISTICS
	t_search_end = dclock();
	d_token->statistics[t_get_box_search] += (t_search_end-t_search_start);
#endif


	//send data
#if AMR_ENABLE_STATISTICS
	t_send_start = dclock();
#endif
	if( !error_flag ){  //no error
		MPI_Send(b->data, box_size, AMR_MPI_DATA_TYPE, client_rank, M_GET_BOX, d_token->nw_comm);   //, &req);   //MPI_REQUEST_NULL
	}else{
		search_error=-1.0;
		MPI_Send(&search_error, 1, AMR_MPI_DATA_TYPE, client_rank, M_GET_BOX, d_token->nw_comm);    //, &req);  //
	}

	free(msg);
	for(i=0; i<ca->dimensions; i++)
		free(cood[i]);
	free(cood);
	//MPI_Wait(&req, MPI_STATUSES_IGNORE);      ///<--- might be performance bottleneck for Isend()  !!!!!!

#if AMR_ENABLE_STATISTICS
	t_total_end = t_send_end = dclock();
	d_token->statistics[t_get_box_send] += (t_send_end - t_send_start);
	d_token->statistics[t_get_total] += (t_total_end - t_total_start);
#endif

	return;
}

#if AMR_ENABLE_STATISTICS
static void process_get_statistics(dn_token * d_token, int source){
	double * vars=NULL;

	vars = (double*)allocate_mem( M_GET_STATISTICS_DN_FIX_DOUBLE*sizeof(double) + 1, "");

	vars[0] = d_token->statistics[t_put_box_search_ts];
	vars[1] = d_token->statistics[t_put_box_search_create_ts];
	vars[2] = d_token->statistics[t_put_box_search_level];
	vars[3] = d_token->statistics[t_put_box_search_create_level];
	vars[4] = d_token->statistics[t_put_box_insert_ht];

	vars[5] = d_token->statistics[t_get_box_search];
	vars[6] = d_token->statistics[t_get_box_search_ht];
	vars[7] = d_token->statistics[t_get_box_send];

	vars[8] = d_token->statistics[t_put_total];
	vars[9] = d_token->statistics[t_get_total];
	vars[10] = d_token->statistics[s_workload];
	vars[11] = d_token->statistics[STAT_SERVER_RECV_INDEX];

	MPI_Send(vars, M_GET_STATISTICS_DN_FIX_DOUBLE, MPI_DOUBLE, source, M_GET_STATISTICS, d_token->nw_comm);

	free(vars);
}
#endif

/* this is the thread executing function, it gets a client's request-msg from threadpool's work-list, reads
 * message types and assigns concrete task to corresponding functions. */
void  dn_process_request(dn_token * d_token, void * msg, int source){
	int msg_type;

	msg_type = ((int*)msg)[0];

	//switch-case on msg-types, those routine functions are responsible for finally free the raw message:
	switch(msg_type){
	case M_NOTIFY_APP_ID:
		process_notify_app_id(d_token, msg);
		break;
	case M_PUSH_BOX:
		process_push_box(d_token, msg, source);
		break;
	case M_GET_BOX:
		process_get_box(d_token, msg, source);
		break;

#if AMR_ENABLE_STATISTICS
	case M_GET_STATISTICS:
		process_get_statistics(d_token, source);
		break;
#endif

	default:
		output2log("server_dn.c; unknown msg_types: %d\n", msg_type);
		free(msg);
	}

}

