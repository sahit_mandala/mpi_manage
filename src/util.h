/*
 * Wenzhao Zhang, wzhang27@ncsu.edu
 * Some util functions, such as list, hashtable
 */

#ifndef AMR_UTIL_H_
#define AMR_UTIL_H_

#include "amr_staging.h"
#include <stdint.h>  //for fast-hash

#define AMR_UTIL_DEBUG 1

#define HT_ARRAY_SIZE_T 100		/* tiny */
#define HT_ARRAY_SIZE_S 1000		/* small */
#define HT_ARRAY_SIZE_M 10000	/* medium */
#define HT_ARRAY_SIZE_L 100000	/* large */

//#define _SEED 4444
#define HASH_KEY_LEN sizeof(int)  //suppose single key size: the sum of a box's coordinates

typedef void * box_hash_table;  /* only for storing symbols */

//for hash-functions
#define mix(h) ({					\
			(h) ^= (h) >> 23;		\
			(h) *= 0x2127599bf4325c37ULL;	\
			(h) ^= (h) >> 47; })

uint64_t fasthash64(const void *buf, size_t len, uint64_t seed);
uint32_t fasthash32(const void *buf, size_t len, uint32_t seed);

typedef struct _ht_list_elem_st{
	int key;  /* hash key, unique, but the actual hash value might be duplicated */
	void * data;  /* will be _symbol struct */
	struct _ht_list_elem_st * next;
}_ht_list_elem;


//*************Start Concurrent Hash Table related function*************
/* size: internal array size; tiny, small, medium, large
 * partition_size: suggest to used the number of threads.
 * */
box_hash_table ht_create_concurrent(int size, int partition_size);

/* return: 0 success, 1 error;
 * key: the sum of a box's coordinates
 * */
int ht_insert_key_concurrent(box * b, box_hash_table ht, AMR_INT key, uint32_t seed); //the only different is that original hash key is provided
int ht_insert_concurrent(box * b, box_hash_table ht, uint32_t seed);

/* return: found data's pointer: success; NULL: error; */
box * ht_search_concurrent(AMR_INT dimension, AMR_INT ** cood, box_hash_table ht, uint32_t seed);  //the only different is that original hash key is provided
box * ht_search_key_concurrent(AMR_INT dimension, AMR_INT ** cood, box_hash_table ht, AMR_INT key, uint32_t seed);

/* return: 0 success, 1 error;
 * only remove the hash-element from the list, not delete the actual data.
 * */
int ht_delete_concurrent(AMR_INT dimension, AMR_INT ** cood, box_hash_table ht);

/*
 * the actual data in each hash-table cell will not be cleaned, users have to do it themselves
 */
void ht_destruct_concurrent(box_hash_table ht);
//*************End Hash Table related function*************




//*************Start Non-Concurrent Hash Table related function*************
/* size: internal array size; tiny, small, medium, large
 * */
box_hash_table ht_create(int size);

/* return: 0 success, 1 error;
 * key: the sum of a box's coordinates
 * */
int ht_insert_key(box * b, box_hash_table ht, AMR_INT key, uint32_t seed); //the only different is that original hash key is provided
int ht_insert(box * b, box_hash_table ht, uint32_t seed);

/* return: found data's pointer: success; NULL: error; */
box * ht_search(AMR_INT dimension, AMR_INT ** cood, box_hash_table ht, uint32_t seed);  //the only different is that original hash key is provided
box * ht_search_key(AMR_INT dimension, AMR_INT ** cood, box_hash_table ht, AMR_INT key, uint32_t seed);

/* return: 0 success, 1 error;
 * only remove the hash-element from the list, not delete the actual data.
 * */
int ht_delete(AMR_INT dimension, AMR_INT ** cood, box_hash_table ht);

/*
 * the actual data in each hash-table cell will not be cleaned, users have to do it themselves
 */
void ht_destruct(box_hash_table ht);
//*************End Hash Table related function*************




void * allocate_mem(unsigned int size, char * msg);
box * allocate_box(AMR_INT dimensions);
void destruct_box(box * b);
AMR_INT ** allocate_coodinates(AMR_INT dimensions);
void output2log(const char * format, ...);
int SDBMHash(char *str);
double dclock();

#endif

