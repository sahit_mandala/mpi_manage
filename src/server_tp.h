
#ifndef AMR_SERVER_THREADPOOL_H_
#define AMR_SERVER_THREADPOOL_H_

#include <pthread.h>
#include "amr_staging.h"
#include "util.h"

struct _threadpool_st;

//typedef void * thr_work_elem;  //thr_work_elem is just a void-pointer, could point to any wrapper struct

/* the function to be executed by a thread.
 * the void-arg should be the threadpool-address, thus the thread can access the "work-elem-list"
 */
typedef void* (*dispatch_fn)(void *);

typedef struct _thr_work_elem_st{
	void * msg;
	int source;

	struct _thr_work_elem_st * next;
}thr_work_elem;

//every server proc has an independent app list
typedef struct _client_app_st{
	int id;
	int dimensions;
	int max_levels;
	int timesteps_len; //default len

	//used by name-nodes: first dimension: an array of timesteps;  second dimension: a list of hash table pointers for each AMR level in a certain TS
	box_hash_table ** ts_level_box;

	//locks are only needed for namenode
	pthread_rwlock_t * ts_locks;   //used to protect each timestep
	pthread_rwlock_t ** level_locks;  //used to protect each timestep's each level
	pthread_rwlock_t ts_level_lock;   //protect the entire "ts_locks"
}client_app;

//name-node token, for threads on a name-node; args passed to pthread_create(), all threads on a node will have the same copy
typedef struct _thr_token_st{
	int * staging_name, staging_name_count;
	int * staging_data, staging_data_count;
	//int * staging_data_leader;
	int staging_data_leader_count;  //how many data-node leaders(how many data-node)
	int staging_data_proc_node;  //how many procs on a data node; here suppose every datanode has the same number of procs

	unsigned long * staging_data_proc_workload;  //totally(not care which app) how many data values(double type) each data node proc holds, only represents the work balance of the metadata held by current name-node, not all name-nodes
	unsigned long * staging_data_node_workload;  //totally(not care which app) how many data values(double type) each data node holds...

	MPI_Comm nw_comm/*new world comm*/;
	MPI_Group nw_group;
	int nw_rank;

	int app_count;
	client_app ** app_set;

	pthread_rwlock_t app_set_lock;
	pthread_rwlock_t * staging_data_proc_workload_locks; //an array of wr_locks, one lock corresponding for each data-node
	pthread_rwlock_t * staging_data_node_workload_locks;

	int tp_size;

#if AMR_ENABLE_STATISTICS
	double ** statistics;  //for stored each resourcepool(or thread)'s statistics, the first dimension is threads, the second dimension is each thread's max&min
	double stat_server_recv;  //server receive msg time
#endif
}thr_token;

typedef struct _resourcepool_st {  //for name-node, a single thread's resource pool
	pthread_mutex_t tp_request_mutex;
	pthread_cond_t tp_request_cond;  /* whether the client-request-queue has element */

	pthread_t tid;
	int poolid;  //ordered by the sequence of the created rscpools, starting from 0

	thr_work_elem * req_head;
	thr_work_elem * req_end;

	int tp_run;

	thr_token * t_token; //every thread share a single copy of thr_token
} resourcepool;

//data-node token, for all the DN procs; no threading on DN, no locks needed
typedef struct _dn_token_st{
	int * staging_name, staging_name_count;
	MPI_Comm nw_comm/*new world comm*/;
	MPI_Group nw_group;
	int nw_rank;
	//int node_leader;  //flag indicates if this proc is a data-node leader
	int app_count;
	client_app ** app_set;
	unsigned long workload; //totally how many data values(double type) this data-node proc holds

#if AMR_ENABLE_STATISTICS
	double * statistics;
#endif
}dn_token;

#define STAT_SERVER_RECV_INDEX 11

resourcepool * create_resourcepool(thr_token * t_token, dispatch_fn, int poolid);

/* add a new work_elem.
 * should be called by a client-request-receiver.
 * return 0:success
 */
int add_tp_work_elem( resourcepool * tp,  void * msg, int source);

/* should be called within a thread */
thr_work_elem * get_tp_work_elem(resourcepool * tp);

void destroy_threadpool(resourcepool * destroyme);

#endif
