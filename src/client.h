/*
 * Wenzhao Zhang, wzhang27@ncsu.edu
 *
 * interface for client side API
 */
#ifndef AMR_CLIENT_H_
#define AMR_CLIENT_H_

#include "amr_staging.h"
#include "util.h"

#define CLIENT_API_SUCCESS 0

//every client process will have a copy of this data structure
typedef struct _client_token_st{
	int * staging_name, staging_name_count; //holds new global ranks of name-node processes
	int *client_procs, client_procs_count;
	int nw_rank, client_rank;
	MPI_Comm nw_comm/*new world comm*/, client_comm;
	MPI_Group nw_group/*new world group*/, client_group;

#if AMR_ENABLE_STATISTICS
	int * staging_data, staging_data_count;  //holds new global ranks of data-node processes, only initialized and used for statistics
#endif
}client_token;

typedef struct _client_app_st{
	int id;
	int dimensions;
	int max_levels;  //starts from 0
}client_app;

/*
 * Client program should call "MPI_Init" first before call this function.
 * This function MUST be called BEFORE any other API functions.
 *
 * ALL client processes should call this function together.
 * MPI_COMM_WORLD should not be used after this init call, use the "nw_comm" in client_token instead
 */
client_token * client_API_init();

/*
 * To register an APP ID with the server;
 * dimension: greater than 1;	max_level: equal or greater than 1.
 * Success: non-NULL client_app pointer.
 *
 * ALL client processes should call this function together.
 */
client_app * register_app(client_token * ct, int dimension, int max_levels);

/* Designed to be called by one process;
 * push a box to the server;
 * before this function returned, should not modify box->data
 * success: 0
 */
int put_box(client_token * ct, client_app * app, int timestep, int level, box * b);

/*
 * Designed to be called by one process;
 * get a box's data from the server;
 * success: 0
 *
 * b: its "cood" must be valid, will be used to perform the get;
 *    if success, its "data" field will be filled with the data of the box
 */
int get_box(client_token * ct, client_app * app, int timestep, int level, box * b);

//print server statistics, mainly about CPU time on each code section
void get_statistics(client_token * ct);

#endif

